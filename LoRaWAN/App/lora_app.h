/**
  ******************************************************************************
  * @file    lora_app.h
  * @author  MCD Application Team
  * @brief   Header of application of the LRWAN Middleware
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under Ultimate Liberty license
  * SLA0044, the "License"; You may not use this file except in compliance with
  * the License. You may obtain a copy of the License at:
  *                             www.st.com/SLA0044
  *
  ******************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __LORA_APP_H__
#define __LORA_APP_H__

#ifdef __cplusplus
extern "C" {
#endif


#include "lora_at.h"



typedef enum {
    APP_NONE_E,
	APP_TICK_TIMER_E,
	APP_TX_DONE_E,
	APP_RX_DONE_E,
	APP_TX_ERR_E,
	APP_RX_ERR_E,
	APP_RE_INIT_P2P_E,
	APP_RESET_E,
	APP_FSET_E,

    APP_NODE_REPORT_E,
    APP_NODE_SEND_REQ_E,
    APP_NODE_HOP_REQ_E,

    
}eAppEventType;


typedef struct {
    int             nParam[6];
    uint32_t        uParam[6];
    char            sData[80];
}sParm80Type;


typedef struct {
    eAppEventType   eEvent;
    sParm80Type     PARM;
}sAppEventType;


typedef enum {

    APP_NONE_S,
    APP_ENTER_S,
    
    APP_CLIENT_INIT_S,
    
    APP_IDLE_INIT_S,
    APP_IDLING_S,

    APP_NODE_SEND_REQ_S,
    APP_NODE_SEND_RSP_S,

    APP_NODE_HOP_REQ_S,
    APP_NODE_HOP_RSP_S,
    
    APP_SLEEP_S,
    APP_SLEEP_WAKE_S,

    APP_RESET_S,

    APP_EXIT_S,
    
}eAppSateType;



typedef struct {
    int             nRetry;
    int             nTickCount;
    uint8_t         uDestNode;
    sParm80Type     PARM;
}sRunType;




typedef ATError_t      (*fNorExec)(void);
typedef ATError_t      (*fNorExecP1)(uint8_t param);
typedef ATError_t      (*fSend)(uint8_t destNode, uint8_t len, char* tx_data);


typedef struct {
    fNorExec        SetReset;
    fNorExec        SetFSet;
    fNorExecP1      SetChannel;
    fNorExecP1      SetRateset;
    fNorExecP1      SetRFPower;
    fSend           SetSend;
    fNorExec        GetRxData;
}sCallBackType;



#define MAX_RX_BUFF_SIZE            40
typedef struct {
    uint8_t         uSeqCount;
    uint8_t         uSrcNode;
    int             nDataSize;
    int             nRssi;
    uint8_t         uDataBuff[MAX_RX_BUFF_SIZE];
}sLastPktType;




#define EVENT_QUEUE_COUNT           3

typedef struct {
    uint8_t             nRetry;
	uint8_t             u8EventIndex;
    uint8_t             u8TxSeqCount;
	sAppEventType       sEventQueue[EVENT_QUEUE_COUNT];

    sLastPktType        LAST_PRE;
    sLastPktType        LAST_TX;
    sLastPktType        LAST_RX;         
    sRunType            RUN;
    sCallBackType       CB;
    uint8_t             uTempBuf[16];

}sAppInfoType;


typedef void (*fSetSleepStart)(int nWakeUpSecond);


void APP_Init(fSetSleepStart fStart);





#ifdef __cplusplus
}
#endif

#endif /*__LORA_APP_H__*/

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
