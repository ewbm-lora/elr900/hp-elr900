/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file    lora_at.c
  * @author  MCD Application Team
  * @brief   AT command API
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under Ultimate Liberty license
  * SLA0044, the "License"; You may not use this file except in compliance with
  * the License. You may obtain a copy of the License at:
  *                             www.st.com/SLA0044
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "platform.h"
#include "lora_at.h"
#include "lora_app.h"
#include "sys_app.h"
#include "stm32_tiny_sscanf.h"
#include "test_rf.h"
#include "adc_if.h"
#include "stm32_seq.h"
#include "utilities_def.h"
#include "radio.h"
#include "flash.h"
#include "stdlib.h"
#include "adc.h"

#ifdef NULL
#undef NULL
#define NULL		0
#endif



static sAppInfoType*        g_pAppInfo = NULL;



/* External variables ---------------------------------------------------------*/
extern void                 SleepStartTimer(void);
extern void                 SetAlarmTime(int nValue);
extern int                  GetAlarmTime(void);
extern int                  GetLoRaMacState(void);
extern bool                 IsReJoinTimerStarted(void);
extern uint32_t             HW_PWM_Led_Test(int nPWMValue);
extern uint32_t             HW_PWM_Led_GetBrightness(void);




/* Private define ------------------------------------------------------------*/
/*!
 * User application data buffer size
 */
#define LORAWAN_APP_DATA_BUFFER_MAX_SIZE            242



void SetATCommandInfo(void *pInfo)
{
    g_pAppInfo = (sAppInfoType*)pInfo;
}



void print_8_02x(uint8_t *pt)
{
    AT_PRINTF("%02X%02X%02X%02X%02X%02X%02X%02X\r\n",
            pt[0], pt[1], pt[2], pt[3], pt[4], pt[5], pt[6], pt[7]);
}






/* Exported functions --------------------------------------------------------*/
ATError_t at_return_ok(const char *param)
{
  return AT_OK;
}

ATError_t at_return_error(const char *param)
{
  return AT_ERROR;
}



/* --------------- General commands --------------- */



ATError_t at_version(const char *param)
{
    AT_PRINTF("VERSION=%03d\r\n", LORA_APP_VERSION);
    return AT_OK;
}



ATError_t at_reset(const char *param)
{
    NVIC_SystemReset();
    
    return AT_OK;
}


ATError_t at_fset(const char *param)
{
    FLASH_EFS_FactoryReset();
    FLASH_EFS_Write();
    
    HAL_Delay(100);
    
    NVIC_SystemReset();
    
    return AT_OK;

}


ATError_t at_test(const char *param)
{
    int			bPwmValue;
    uint16_t	uAdcRead[2];

    if(param != NULL && param[0] != NULL) {
        
        if(param[0] == 'S' || param[0] == 's') {
            // Sense Request
            adc_temp_read(uAdcRead);
            AT_PRINTF("ADC %d,  %d \r\n", uAdcRead[0], uAdcRead[1]);
            return AT_OK;
        }
    
        if (tiny_sscanf(param, "%d", &bPwmValue) != 1) {
            return AT_ERROR;
        }
        AT_PRINTF("Test PWM =%d\r\n", bPwmValue);
        HW_PWM_Led_Test(bPwmValue);
        
        return AT_OK;
    }
    else {
        AT_PRINTF("Test PWM =%d\r\n", HW_PWM_Led_GetBrightness());
        return AT_OK;
    }
   
}


ATError_t at_verbose_get(const char *param)
{
    AT_PRINTF("DBGL=%d\r\n", GetFLASH()->uDbgLevel);
    return AT_OK;
}


ATError_t at_verbose_set(const char *param)
{
    uint8_t uDbgLevel;
        
    if(param != NULL && param[0] != NULL) {
        if (tiny_sscanf(param, "%u", &uDbgLevel) != 1) {
            return AT_ERROR;
        }

        GetFLASH()->uDbgLevel = !!uDbgLevel;
        UTIL_ADV_TRACE_SetVerboseLevel(GetFLASH()->uDbgLevel);
        FLASH_EFS_Write();
        AT_PRINTF("DBGL=%d\r\n", GetFLASH()->uDbgLevel);
        return AT_OK;
    }
   
    return AT_PARAM_ERROR;

}




ATError_t at_get_p2p_channel(const char *param)
{
    AT_PRINTF("CHANNEL=%d\r\n", GetFLASH()->u8Channel);
    return AT_OK;
}



ATError_t at_set_p2p_channel(const char *param)
{
    uint8_t u8Channel;
    
    if(param != NULL && param[0] != NULL) {
        if (tiny_sscanf(param, "%u", &u8Channel) != 1) {
            return AT_ERROR;
        }

        if(u8Channel >= 64) {
            return AT_PARAM_ERROR;
        }

        AT_PRINTF("CHANNEL=%d\r\n", u8Channel);
        if(g_pAppInfo->CB.SetChannel) {
            return g_pAppInfo->CB.SetChannel(u8Channel);
        }
    }
   
    return AT_PARAM_ERROR;
}




ATError_t at_get_p2p_rateset(const char *param)
{
    AT_PRINTF("RATESET=%d\r\n", GetFLASH()->u8Rateset);
    return AT_OK;
}



ATError_t at_set_p2p_rateset(const char *param)
{
    uint8_t u8Rateset;
    
    if(param != NULL && param[0] != NULL) {
        if (tiny_sscanf(param, "%u", &u8Rateset) != 1) {
            return AT_ERROR;
        }

        AT_PRINTF("RATESET=%d\r\n", u8Rateset);
        
        if(g_pAppInfo->CB.SetRateset) {
            return g_pAppInfo->CB.SetRateset(u8Rateset);
        }
    }
   
    return AT_PARAM_ERROR;
}




ATError_t at_get_p2p_node_id(const char *param)
{
    AT_PRINTF("NODEID=%d\r\n", GetFLASH()->u8NodeId);
    return AT_OK;
}



ATError_t at_set_p2p_node_id(const char *param)
{
    uint8_t u8NodeID;
    
    if(param != NULL && param[0] != NULL) {
        if (tiny_sscanf(param, "%u", &u8NodeID) != 1) {
            return AT_ERROR;
        }
        AT_PRINTF("NODEID=%d\r\n", u8NodeID);
        GetFLASH()->u8NodeId = u8NodeID;
        
        FLASH_EFS_Write();
        return AT_OK;
    }
    return AT_PARAM_ERROR;
}



ATError_t at_set_p2p_reset(const char *param)
{
    if(g_pAppInfo->CB.SetReset != NULL) {
        return g_pAppInfo->CB.SetReset();
    }
    return AT_ERROR;
}



ATError_t at_set_p2p_fset(const char *param)
{
    if(g_pAppInfo->CB.SetFSet != NULL) {
        return g_pAppInfo->CB.SetFSet();
    }
    return AT_ERROR;
}




ATError_t at_get_p2p_rfpower(const char *param)
{
    AT_PRINTF("RFPOWER=%d\r\n", GetFLASH()->u8Power);
    return AT_OK;
}




ATError_t at_set_p2p_rfpower(const char *param)
{
    uint8_t u8Power;
    if(param == NULL || param[0] == NULL) {
        return AT_PARAM_ERROR;
    }

    if (tiny_sscanf(param, "%u", &u8Power) != 1) {
        return AT_PARAM_ERROR;
    }

    if(g_pAppInfo->CB.SetRFPower != NULL) {
        AT_PRINTF("RFPOWER=%d\r\n", u8Power);
        return g_pAppInfo->CB.SetRFPower(u8Power);
    }
    return AT_ERROR;
}




ATError_t at_set_p2p_send(const char *param)
{
    char        sToken[50];
    uint16_t    destNode;
    int         size;
    uint8_t     len;
  
    if(param == NULL || param[0] == NULL) {
        return AT_PARAM_ERROR;
    }

    char *sStart = (char*)param;
    char *sParse = strstr(sStart, ",");
    if(sParse == NULL) {
        return AT_PARAM_ERROR;
    }

    size = (uint8_t)(sParse - sStart);
    memcpy(sToken, sStart, size);
    sToken[size] = NULL;
    destNode = atoi(sToken);
    

    sStart += (size + 1);
    sParse = strstr(sStart, ",");
    if(sParse == NULL) {
        return AT_PARAM_ERROR;
    }
    size = (uint8_t)(sParse - sStart);
    memcpy(sToken, sStart, size);
    sToken[size] = NULL;
    len = atoi(sToken);
    
    sStart += (size + 1);
    if(g_pAppInfo->CB.SetSend != NULL) {
       return g_pAppInfo->CB.SetSend(destNode, len, sStart);
    }

    return AT_ERROR;

}



ATError_t at_set_p2p_bdload(const char *param)
{
    sEfsItemType *pEfsData;
    pEfsData = GetFLASH();
    
    pEfsData->dwResetReson = 0xFF05FF04;
    
    FLASH_EFS_Write();

    HAL_Delay(100);
    
    NVIC_SystemReset();
    
    return AT_OK;
  
}


ATError_t at_get_p2p_deveui(const char *param)
{
    sEfsItemType *pEfsData;
    pEfsData = GetFLASH();
   

    AT_PRINTF("DEVEUI = "); print_8_02x(pEfsData->uDevEui);
    return AT_OK;
}


ATError_t at_set_p2p_deveui(const char *param)
{
    uint8_t         devEui[8];
    sEfsItemType*   pEfsData;

    
    pEfsData = GetFLASH();

    
    if (tiny_sscanf(param, "%02x%02x%02x%02x%02x%02x%02x%02x",
                    &devEui[0], &devEui[1], &devEui[2], &devEui[3],
                    &devEui[4], &devEui[5], &devEui[6], &devEui[7]) != 8)
    {
        return AT_PARAM_ERROR;
    }
    
    
    memcpy(pEfsData->uDevEui, devEui, 8);
    FLASH_EFS_Write();
    
    AT_PRINTF("DEVEUI = "); print_8_02x(devEui);
    
    return AT_OK;
}





/* USER CODE END PrFD */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
