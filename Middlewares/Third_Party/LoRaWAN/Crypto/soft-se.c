/*!
 * \file      soft-se.c
 *
 * \brief     Secure Element software implementation
 *
 * \copyright Revised BSD License, see section \ref LICENSE.
 *
 * \code
 *                ______                              _
 *               / _____)             _              | |
 *              ( (____  _____ ____ _| |_ _____  ____| |__
 *               \____ \| ___ |    (_   _) ___ |/ ___)  _ \
 *               _____) ) ____| | | || |_| ____( (___| | | |
 *              (______/|_____)_|_|_| \__)_____)\____)_| |_|
 *              (C)2020 Semtech
 *
 *               ___ _____ _   ___ _  _____ ___  ___  ___ ___
 *              / __|_   _/_\ / __| |/ / __/ _ \| _ \/ __| __|
 *              \__ \ | |/ _ \ (__| ' <| _| (_) |   / (__| _|
 *              |___/ |_/_/ \_\___|_|\_\_| \___/|_|_\\___|___|
 *              embedded.connectivity.solutions===============
 *
 * \endcode
 *
 */

/**
  ******************************************************************************
  *
  *          Portions COPYRIGHT 2020 STMicroelectronics
  *
  * @file    soft-se.c
  * @author  MCD Application Team
  * @brief   Secure Element software implementation
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include <stdlib.h>
#include <stdint.h>

#include "lorawan_conf.h"  /* LORAWAN_KMS */
#include "radio.h"         /* needed for Random */
#include "utilities.h"
#include "mw_log_conf.h"   /* needed for MW_LOG */
#if (!defined (LORAWAN_KMS) || (LORAWAN_KMS == 0))
#include "lorawan_aes.h"
#include "cmac.h"
#else /* LORAWAN_KMS == 1 */
#include "kms_if.h"
#endif /* LORAWAN_KMS */

#include "LoRaMacHeaderTypes.h"

#include "secure-element.h"
#include "secure-element-nvm.h"
#include "se-identity.h"
#include "flash.h"

/* Private constants ---------------------------------------------------------*/
/*!
 * MIC computation offset
 * \remark required for 1.1.x support
 */
#define CRYPTO_MIC_COMPUTATION_OFFSET ( JOIN_REQ_TYPE_SIZE\
                                        + LORAMAC_JOIN_EUI_FIELD_SIZE + DEV_NONCE_SIZE + LORAMAC_MHDR_FIELD_SIZE )

#if (!defined (LORAWAN_KMS) || (LORAWAN_KMS == 0))
#else /* LORAWAN_KMS == 1 */
#define DERIVED_OBJECT_HANDLE_RESET_VAL      0x0UL
#define PAYLOAD_MAX_SIZE     270UL  /* 270 PHYPayload: 1+(22+1+242)+4 */
#endif /* LORAWAN_KMS */

/* Private macro -------------------------------------------------------------*/
/*!
 * Hex 8 split buffer
 */
#define HEX8(X)   X[0], X[1], X[2], X[3], X[4], X[5], X[6], X[7]

/*!
 * Hex 16 split buffer
 */
#define HEX16(X)  HEX8(X), X[8], X[9], X[10], X[11], X[12], X[13], X[14], X[15]

/* Private Types ---------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
/*!
 * Secure element context
 */
static SecureElementNvmData_t* SeNvm;

#if (!defined (LORAWAN_KMS) || (LORAWAN_KMS == 0))
#else /* LORAWAN_KMS == 1 */
/* WARNING: Should be modified at the end of product development */
static const CK_ULONG GlobalTemplateLabel = 0x444E524CU;

/*
 * Intermediate buffer used for two reasons:
 * - align to 32 bits and
 * - for Cmac combine InitVector + input buff
 */
static uint8_t input_align_combined_buf[PAYLOAD_MAX_SIZE + SE_KEY_SIZE] ALIGN(4);

static uint8_t output_align[PAYLOAD_MAX_SIZE] ALIGN(4);

static uint8_t tag[SE_KEY_SIZE] ALIGN(4) = {0};
#endif /* LORAWAN_KMS */

/* Private functions prototypes ---------------------------------------------------*/
#if (!defined (LORAWAN_KMS) || (LORAWAN_KMS == 0))
static SecureElementStatus_t GetKeyByID( KeyIdentifier_t keyID, Key_t **keyItem );
#else /* LORAWAN_KMS == 1 */
static SecureElementStatus_t GetKeyIndexByID( KeyIdentifier_t keyID, CK_OBJECT_HANDLE *keyIndex );

static SecureElementStatus_t GetSpecificLabelByID( KeyIdentifier_t keyID, uint32_t *specificLabel );

static SecureElementStatus_t DeleteAllDynamicKeys( void );
#endif /* LORAWAN_KMS */



static SecureElementStatus_t ComputeCmac(uint8_t *micBxBuffer, uint8_t *buffer, uint16_t size, KeyIdentifier_t keyID,
                                         uint32_t *cmac);


#if (!defined (LORAWAN_KMS) || (LORAWAN_KMS == 0))
/*
 * Gets key item from key list.
 *
 * \param[IN]  keyID          - Key identifier
 * \param[OUT] keyItem        - Key item reference
 * \retval                    - Status of the operation
 */
static SecureElementStatus_t GetKeyByID( KeyIdentifier_t keyID, Key_t** keyItem )
{
    for( uint8_t i = 0; i < NUM_OF_KEYS; i++ )
    {
        if( SeNvm->KeyList[i].KeyID == keyID )
        {
            *keyItem = &( SeNvm->KeyList[i] );
            return SECURE_ELEMENT_SUCCESS;
        }
    }
    return SECURE_ELEMENT_ERROR_INVALID_KEY_ID;
}

#else /* LORAWAN_KMS == 1 */

/*
 * Gets key Index from key list in KMS table
 *
 * \param[IN]    keyID                    - Key identifier
 * \param[OUT] keyIndex             - Key item reference
 * \retval                                        - Status of the operation
 */
static SecureElementStatus_t GetKeyIndexByID( KeyIdentifier_t keyID, CK_OBJECT_HANDLE *keyIndex )
{
    for (uint8_t i = 0; i < NUM_OF_KEYS; i++)
    {
        if (SeNvm->KeyList[i].KeyID == keyID)
        {
            *keyIndex = SeNvm->KeyList[i].Object_Index;
            return SECURE_ELEMENT_SUCCESS;
        }
    }
    return SECURE_ELEMENT_ERROR_INVALID_KEY_ID;
}

static SecureElementStatus_t GetSpecificLabelByID( KeyIdentifier_t keyID, uint32_t *specificLabel )
{
    SecureElementStatus_t retval = SECURE_ELEMENT_SUCCESS;
    switch (keyID)
    {
        case APP_KEY:
            *specificLabel = 0x5F505041U;
            break;
        case NWK_KEY:
            *specificLabel = 0x5F4B574EU;
            break;
        case NWK_S_KEY:
            *specificLabel = 0x534B574EU;
            break;
        case APP_S_KEY:
            *specificLabel = 0x53505041U;
            break;
        case MC_ROOT_KEY:
            *specificLabel = 0x5452434DU;
            break;
        case MC_KE_KEY:
            *specificLabel = 0x454B434DU;
            break;
        case MC_KEY_0:
            *specificLabel = 0x304B434DU;
            break;
        case MC_APP_S_KEY_0:
            *specificLabel = 0x3053414DU;
            break;
        case MC_NWK_S_KEY_0:
            *specificLabel = 0x30534E4DU;
            break;
#if ( LORAMAC_MAX_MC_CTX > 1 )
        case MC_KEY_1:
            *specificLabel = 0x314B434DU;
            break;
        case MC_APP_S_KEY_1:
            *specificLabel = 0x3153414DU;
            break;
        case MC_NWK_S_KEY_1:
            *specificLabel = 0x31534E4DU;
            break;
        case MC_KEY_2:
            *specificLabel = 0x324B434DU;
            break;
        case MC_APP_S_KEY_2:
            *specificLabel = 0x3253414DU;
            break;
        case MC_NWK_S_KEY_2:
            *specificLabel = 0x32534E4DU;
            break;
        case MC_KEY_3:
            *specificLabel = 0x334B434DU;
            break;
        case MC_APP_S_KEY_3:
            *specificLabel = 0x3353414DU;
            break;
        case MC_NWK_S_KEY_3:
            *specificLabel = 0x33534E4DU;
            break;
#endif /* LORAMAC_MAX_MC_CTX > 1 */
        default:
            retval = SECURE_ELEMENT_ERROR_INVALID_KEY_ID;
            break;
    }
    return retval;
}

static SecureElementStatus_t DeleteAllDynamicKeys( void )
{
    CK_RV rv;
    CK_SESSION_HANDLE session;
    CK_FLAGS session_flags = CKF_SERIAL_SESSION;  /* Read ONLY session */
    CK_OBJECT_HANDLE hObject[NUM_OF_KEYS];
    uint32_t ulCount = 0;

    /* Open session with KMS */
    rv = C_OpenSession(0,  session_flags, NULL, 0, &session);

    /* Get all keys handle */
    if (rv == CKR_OK)
    {
        rv = C_FindObjectsInit(session, NULL, 0);
    }

    /* Find all existing keys handle */
    if (rv == CKR_OK)
    {
        rv = C_FindObjects(session, hObject, NUM_OF_KEYS, (CK_ULONG *) &ulCount);
    }

    if (rv == CKR_OK)
    {
        rv = C_FindObjectsFinal(session);
    }

    if (ulCount <= NUM_OF_KEYS)
    {
        for (uint8_t i = 0; i < ulCount; i++)
        {
            /* Exclude all Embedded keys */
            if (hObject[i] > LAST_KMS_KEY_OBJECT_HANDLE)
            {
                if (rv == CKR_OK)
                {
                    rv = C_DestroyObject(session, hObject[i]);
                }
            }
        }
    }

    /* Close sessions */
    if (session > 0)
    {
        (void)C_CloseSession(session);
    }

    if (rv != CKR_OK)
    {
        return SECURE_ELEMENT_ERROR;
    }
    return SECURE_ELEMENT_SUCCESS;
}
#endif /* LORAWAN_KMS */

/*
 * Computes a CMAC of a message using provided initial Bx block
 *
 *  cmac = aes128_cmac(keyID, blocks[i].Buffer)
 *
 * \param[IN]  micBxBuffer    - Buffer containing the initial Bx block
 * \param[IN]  buffer         - Data buffer
 * \param[IN]  size           - Data buffer size
 * \param[IN]  keyID          - Key identifier to determine the AES key to be used
 * \param[OUT] cmac           - Computed cmac
 * \retval                    - Status of the operation
 */
static SecureElementStatus_t ComputeCmac( uint8_t* micBxBuffer, uint8_t* buffer, uint16_t size, KeyIdentifier_t keyID,
                                          uint32_t* cmac )
{
    if( ( buffer == NULL ) || ( cmac == NULL ) )
    {
        return SECURE_ELEMENT_ERROR_NPE;
    }

#if (!defined (LORAWAN_KMS) || (LORAWAN_KMS == 0))
    uint8_t Cmac[16];
    AES_CMAC_CTX aesCmacCtx[1];

    AES_CMAC_Init( aesCmacCtx );

    Key_t*                keyItem;
    SecureElementStatus_t retval = GetKeyByID( keyID, &keyItem );

    if( retval == SECURE_ELEMENT_SUCCESS )
    {
        AES_CMAC_SetKey( aesCmacCtx, keyItem->KeyValue );

        if( micBxBuffer != NULL )
        {
            AES_CMAC_Update( aesCmacCtx, micBxBuffer, 16 );
        }

        AES_CMAC_Update( aesCmacCtx, buffer, size );

        AES_CMAC_Final( Cmac, aesCmacCtx );

        // Bring into the required format
        *cmac = ( uint32_t )( ( uint32_t ) Cmac[3] << 24 | ( uint32_t ) Cmac[2] << 16 | ( uint32_t ) Cmac[1] << 8 |
                              ( uint32_t ) Cmac[0] );
    }
#else /* LORAWAN_KMS == 1 */
    CK_RV rv;
    CK_SESSION_HANDLE session;
    CK_FLAGS session_flags = CKF_SERIAL_SESSION;    /* Read ONLY session */
    uint32_t tag_lenth = 0;
    CK_OBJECT_HANDLE key_handle ;

    /* AES CMAC Authentication variables */
    CK_MECHANISM aes_cmac_mechanism = { CKM_AES_CMAC, (CK_VOID_PTR)NULL, 0 };

    SecureElementStatus_t retval = GetKeyIndexByID(keyID, &key_handle);
    if (retval != SECURE_ELEMENT_SUCCESS)
    {
        return retval;
    }

    /* Open session with KMS */
    rv = C_OpenSession(0,    session_flags, NULL, 0, &session);

    /* Configure session to Authentication message in AES CMAC with settings included into the mechanism */
    if (rv == CKR_OK)
    {
        rv = C_SignInit(session, &aes_cmac_mechanism, key_handle);
    }

    /* Encrypt clear message */
    if (rv == CKR_OK)
    {
        /* work around : need to double-check if possible to use micBxBuffer as IV for Sign */
        if (micBxBuffer != NULL)
        {
            memcpy1((uint8_t *) &input_align_combined_buf[0], (uint8_t *) micBxBuffer, SE_KEY_SIZE);
            memcpy1((uint8_t *) &input_align_combined_buf[SE_KEY_SIZE], (uint8_t *) buffer, size);
        }
        else
        {
            memcpy1((uint8_t *) &input_align_combined_buf[0], (uint8_t *) buffer, size);
        }
    }

    if (rv == CKR_OK)
    {
        if (micBxBuffer != NULL)
        {
            rv = C_Sign(session, (CK_BYTE_PTR)&input_align_combined_buf[0], size + SE_KEY_SIZE, &tag[0],
                        (CK_ULONG_PTR)&tag_lenth);
        }
        else
        {
            rv = C_Sign(session, (CK_BYTE_PTR)&input_align_combined_buf[0], size, &tag[0],
                        (CK_ULONG_PTR)&tag_lenth);
        }
    }

    /* Close session with KMS */
    (void)C_CloseSession(session);

    /* combine to a 32bit authentication word (MIC) */
    *cmac = (uint32_t)((uint32_t) tag[3] << 24 | (uint32_t) tag[2] << 16 | (uint32_t) tag[1] << 8 |
                       (uint32_t) tag[0]);

    if (rv != CKR_OK)
    {
        retval = SECURE_ELEMENT_ERROR;
    }
#endif /* LORAWAN_KMS */
    return retval;
}


/* Exported functions ---------------------------------------------------------*/

/*
 * API functions
 */
/* ST_WORKAROUND: Add unique ID callback as input parameter */
SecureElementStatus_t SecureElementInit( SecureElementNvmData_t *nvm, SecureElementGetUniqueId seGetUniqueId )
{
    Key_t *keyItem;
    SecureElementStatus_t retval = SECURE_ELEMENT_ERROR;
    SecureElementNvmData_t seNvmInit =
    {
        /*!
        * end-device IEEE EUI (big endian)
        *
        * \remark In this application the value is automatically generated by
        *         calling BoardGetUniqueId function
        */
        .DevEui = LORAWAN_DEVICE_EUI,
        /*!
        * App/Join server IEEE EUI (big endian)
        */
        .AppEui = LORAWAN_APP_EUI,
        /*!
        * LoRaWAN key list
        */
        .KeyList = SOFT_SE_KEY_LIST
    };

    if( nvm == NULL ){
        return SECURE_ELEMENT_ERROR_NPE;
    }

    // Initialize nvm pointer
    SeNvm = nvm;
    
    // Initialize data
    memcpy1( ( uint8_t* )SeNvm, ( uint8_t* )&seNvmInit, sizeof( seNvmInit ) );

    memcpy(SeNvm->DevEui, FLASH_EFS_GetMemory()->DevEui, 8);
    memcpy(SeNvm->AppEui, FLASH_EFS_GetMemory()->AppEui, 8);
    
    retval = GetKeyByID(APP_KEY, &keyItem);
    if (retval == SECURE_ELEMENT_SUCCESS) {
        memcpy(keyItem->KeyValue, FLASH_EFS_GetMemory()->AppKey, 16);
	}

    retval = GetKeyByID(NWK_S_KEY, &keyItem);
    if (retval == SECURE_ELEMENT_SUCCESS) {
        memcpy(keyItem->KeyValue, FLASH_EFS_GetMemory()->NwkSKey, 16);
	}

    retval = GetKeyByID(APP_S_KEY, &keyItem);
    if (retval == SECURE_ELEMENT_SUCCESS) {
        memcpy(keyItem->KeyValue, FLASH_EFS_GetMemory()->AppSKey, 16);
	}
        
    return SECURE_ELEMENT_SUCCESS;
}





/* ST_WORKAROUND_BEGIN: Add KMS specific functions */
SecureElementStatus_t SecureElementGetKeyByID( KeyIdentifier_t keyID, Key_t **keyItem )
{
    for (uint8_t i = 0; i < NUM_OF_KEYS; i++)
    {
        if (SeNvm->KeyList[i].KeyID == keyID)
        {
            *keyItem = &(SeNvm->KeyList[i]);
            return SECURE_ELEMENT_SUCCESS;
        }
    }
    return SECURE_ELEMENT_ERROR_INVALID_KEY_ID;
}



SecureElementStatus_t SecureElementSetKey( KeyIdentifier_t keyID, uint8_t* key )
{

    if( key == NULL ) {
        return SECURE_ELEMENT_ERROR_NPE;
    }


    for( uint8_t i = 0; i < NUM_OF_KEYS; i++ ) {
        if( SeNvm->KeyList[i].KeyID == keyID ) {
            memcpy1( SeNvm->KeyList[i].KeyValue, key, SE_KEY_SIZE );
            return SECURE_ELEMENT_SUCCESS;
        }
    }

    return SECURE_ELEMENT_ERROR_INVALID_KEY_ID;
}




SecureElementStatus_t SecureElementComputeAesCmac( uint8_t* micBxBuffer, uint8_t* buffer, uint16_t size,
                                                   KeyIdentifier_t keyID, uint32_t* cmac )
{
    if( keyID >= LORAMAC_CRYPTO_MULTICAST_KEYS )
    {
        // Never accept multicast key identifier for cmac computation
        return SECURE_ELEMENT_ERROR_INVALID_KEY_ID;
    }

    return ComputeCmac( micBxBuffer, buffer, size, keyID, cmac );
}

SecureElementStatus_t SecureElementVerifyAesCmac( uint8_t* buffer, uint16_t size, uint32_t expectedCmac,
                                                  KeyIdentifier_t keyID )
{
    if( buffer == NULL )
    {
        return SECURE_ELEMENT_ERROR_NPE;
    }

    SecureElementStatus_t retval   = SECURE_ELEMENT_ERROR;
#if (!defined (LORAWAN_KMS) || (LORAWAN_KMS == 0))
    uint32_t              compCmac = 0;
    retval                         = ComputeCmac( NULL, buffer, size, keyID, &compCmac );
    if( retval != SECURE_ELEMENT_SUCCESS )
    {
        return retval;
    }

    if( expectedCmac != compCmac )
    {
        retval = SECURE_ELEMENT_FAIL_CMAC;
    }
#else /* LORAWAN_KMS == 1 */
    CK_RV rv;
    CK_SESSION_HANDLE session;
    CK_FLAGS session_flags = CKF_SERIAL_SESSION;    /* Read ONLY session */
    CK_OBJECT_HANDLE object_handle;

    if (buffer == NULL)
    {
        return SECURE_ELEMENT_ERROR_NPE;
    }

    /* AES CMAC Authentication variables */
    CK_MECHANISM aes_cmac_mechanism = { CKM_AES_CMAC, (CK_VOID_PTR)NULL, 0 };

    retval = GetKeyIndexByID(keyID, &object_handle);
    if (retval != SECURE_ELEMENT_SUCCESS)
    {
        return retval;
    }

    /* Open session with KMS */
    rv = C_OpenSession(0,    session_flags, NULL, 0, &session);

    /* Configure session to Verify the message in AES CMAC with settings included into the mechanism */
    if (rv == CKR_OK)
    {
        rv = C_VerifyInit(session, &aes_cmac_mechanism, object_handle);
    }

    /* Verify the message */
    if (rv == CKR_OK)
    {
        memcpy1(input_align_combined_buf, buffer, size);
        rv = C_Verify(session, (CK_BYTE_PTR)input_align_combined_buf, size, (CK_BYTE_PTR)&expectedCmac, 4);
    }

    (void)C_CloseSession(session);

    if (rv != CKR_OK)
    {
        retval = SECURE_ELEMENT_ERROR;
    }

#endif /* LORAWAN_KMS */

    return retval;
}

SecureElementStatus_t SecureElementAesEncrypt( uint8_t* buffer, uint16_t size, KeyIdentifier_t keyID,
                                               uint8_t* encBuffer )
{
    if( buffer == NULL || encBuffer == NULL )
    {
        return SECURE_ELEMENT_ERROR_NPE;
    }

    // Check if the size is divisible by 16,
    if( ( size % 16 ) != 0 )
    {
        return SECURE_ELEMENT_ERROR_BUF_SIZE;
    }

    lorawan_aes_context aesContext;
    memset1( aesContext.ksch, '\0', 240 );

    Key_t*                pItem;
    SecureElementStatus_t retval = GetKeyByID( keyID, &pItem );

    if( retval == SECURE_ELEMENT_SUCCESS )
    {
        lorawan_aes_set_key(pItem->KeyValue, 16, &aesContext);

        uint8_t block = 0;

        while( size != 0 )
        {
            lorawan_aes_encrypt(&buffer[block], &encBuffer[block], &aesContext);
            block = block + 16;
            size  = size - 16;
        }
    }

    return retval;
}



SecureElementStatus_t SecureElementDeriveAndStoreKey( uint8_t* input, KeyIdentifier_t rootKeyID,
                                                      KeyIdentifier_t targetKeyID )
{
    uint8_t key[16] = { 0 };
    if( input == NULL )
    {
        return SECURE_ELEMENT_ERROR_NPE;
    }

    SecureElementStatus_t retval  = SECURE_ELEMENT_ERROR;
    
    // Derive key
    retval = SecureElementAesEncrypt( input, 16, rootKeyID, key );
    if( retval != SECURE_ELEMENT_SUCCESS )
    {
        return retval;
    }

    // Store key
    retval = SecureElementSetKey( targetKeyID, key );
    if( retval != SECURE_ELEMENT_SUCCESS )
    {
        return retval;
    }

    return SECURE_ELEMENT_SUCCESS;
}



SecureElementStatus_t SecureElementProcessJoinAccept( JoinReqIdentifier_t joinReqType, uint8_t* joinEui,
                                                      uint16_t devNonce, uint8_t* encJoinAccept,
                                                      uint8_t encJoinAcceptSize, uint8_t* decJoinAccept,
                                                      uint8_t* versionMinor )
{
    if( ( encJoinAccept == NULL ) || ( decJoinAccept == NULL ) || ( versionMinor == NULL ) )
    {
        return SECURE_ELEMENT_ERROR_NPE;
    }

    // Check that frame size isn't bigger than a JoinAccept with CFList size
    if( encJoinAcceptSize > LORAMAC_JOIN_ACCEPT_FRAME_MAX_SIZE )
    {
        return SECURE_ELEMENT_ERROR_BUF_SIZE;
    }

    // Determine decryption key
    KeyIdentifier_t encKeyID = APP_KEY;

    /* ST_WORKAROUND_BEGIN: Keep NWK_KEY if 1.1.x keys are not defined */
    memcpy1( decJoinAccept, encJoinAccept, encJoinAcceptSize );

    // Decrypt JoinAccept, skip MHDR
    if( SecureElementAesEncrypt( encJoinAccept + LORAMAC_MHDR_FIELD_SIZE, encJoinAcceptSize - LORAMAC_MHDR_FIELD_SIZE,
                                 encKeyID, decJoinAccept + LORAMAC_MHDR_FIELD_SIZE ) != SECURE_ELEMENT_SUCCESS )
    {
        return SECURE_ELEMENT_FAIL_ENCRYPT;
    }

    *versionMinor = ( ( decJoinAccept[11] & 0x80 ) == 0x80 ) ? 1 : 0;

    uint32_t mic = 0;

    mic = ( ( uint32_t ) decJoinAccept[encJoinAcceptSize - LORAMAC_MIC_FIELD_SIZE] << 0 );
    mic |= ( ( uint32_t ) decJoinAccept[encJoinAcceptSize - LORAMAC_MIC_FIELD_SIZE + 1] << 8 );
    mic |= ( ( uint32_t ) decJoinAccept[encJoinAcceptSize - LORAMAC_MIC_FIELD_SIZE + 2] << 16 );
    mic |= ( ( uint32_t ) decJoinAccept[encJoinAcceptSize - LORAMAC_MIC_FIELD_SIZE + 3] << 24 );

    //  - Header buffer to be used for MIC computation
    //        - LoRaWAN 1.0.x : micHeader = [MHDR(1)]
    //        - LoRaWAN 1.1.x : micHeader = [JoinReqType(1), JoinEUI(8), DevNonce(2), MHDR(1)]

    // Verify mic
    if( *versionMinor == 0 )
    {
        // For LoRaWAN 1.0.x
        //   cmac = aes128_cmac(NwkKey, MHDR |  JoinNonce | NetID | DevAddr | DLSettings | RxDelay | CFList |
        //   CFListType)
        if( SecureElementVerifyAesCmac( decJoinAccept, ( encJoinAcceptSize - LORAMAC_MIC_FIELD_SIZE ), mic, APP_KEY ) !=
            SECURE_ELEMENT_SUCCESS )
        {
            return SECURE_ELEMENT_FAIL_CMAC;
        }
    }
    else
    {
        return SECURE_ELEMENT_ERROR_INVALID_LORAWAM_SPEC_VERSION;
    }

    return SECURE_ELEMENT_SUCCESS;
}

SecureElementStatus_t SecureElementRandomNumber( uint32_t* randomNum )
{
    if( randomNum == NULL )
    {
        return SECURE_ELEMENT_ERROR_NPE;
    }
    *randomNum = Radio.Random();
    return SECURE_ELEMENT_SUCCESS;
}

SecureElementStatus_t SecureElementSetDevEui( uint8_t* devEui )
{
    if( devEui == NULL )
    {
        return SECURE_ELEMENT_ERROR_NPE;
    }
    memcpy1( SeNvm->DevEui, devEui, SE_EUI_SIZE );
    return SECURE_ELEMENT_SUCCESS;
}

uint8_t* SecureElementGetDevEui( void )
{
    return SeNvm->DevEui;
}

SecureElementStatus_t SecureElementSetAppEui( uint8_t* AppEui )
{
    if( AppEui == NULL )
    {
        return SECURE_ELEMENT_ERROR_NPE;
    }
    memcpy1( SeNvm->AppEui, AppEui, SE_EUI_SIZE );
    return SECURE_ELEMENT_SUCCESS;
}

uint8_t* SecureElementGetAppEui( void )
{
    return SeNvm->AppEui;
}


uint8_t *SecureElementGetAppKey(void)
{
    Key_t *keyItem = NULL;
    GetKeyByID(APP_KEY, &keyItem);

    if(keyItem != NULL) {
    	return keyItem->KeyValue;
    }
    else {
    	return NULL;
    }
}



uint8_t *SecureElementGetNwkSKey(void)
{
    Key_t *keyItem = NULL;
    GetKeyByID(NWK_S_KEY, &keyItem);

    if(keyItem != NULL) {
    	return keyItem->KeyValue;
    }
    else {
    	return NULL;
    }
}



uint8_t *SecureElementGetAppSKey(void)
{
    Key_t *keyItem = NULL;
    GetKeyByID(APP_S_KEY, &keyItem);

    if(keyItem != NULL) {
    	return keyItem->KeyValue;
    }
    else {
    	return NULL;
    }
}



