
#ifndef __FLASH_H__
#define __FLASH_H__



#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/





typedef enum {
  FLASHIF_OK = 0,
  FLASHIF_ERASEKO,
  FLASHIF_WRITINGCTRL_ERROR,
  FLASHIF_WRITING_ERROR,
  FLASHIF_PROTECTION_ERRROR
}sFlashErrType;


/* If the EFS Changed -------------------*/
#define EFS_INIT_DONE       0x58762539



typedef struct {
    uint32_t	dwResetReson;
    uint32_t    dwEfsInit;
    uint32_t    dwEfsSize;
   
    uint8_t     uDbgLevel;
    uint8_t     uTrimValA;
    uint8_t     uTrimValB;
    
    uint8_t     u8NodeId;
    uint8_t     u8HopLevel;
    uint8_t     u8HopDelay;
    uint8_t     u8Channel;
    uint8_t     u8Power;
    uint8_t     u8Rateset;

    uint8_t		uDevEui[8];

}sEfsItemType;

void			MX_FLASH_Init(void);

sEfsItemType	*GetFLASH(void);
void            FLASH_EFS_FactoryReset();
uint32_t		FLASH_EFS_Write(void);
uint32_t		FLASH_EFS_Read(void);


#ifdef __cplusplus
}
#endif

#endif /* __FLASH_H__ */


