
#include "platform.h"
#include "sys_app.h"
#include "lora_p2p.h"
#include "radio.h"
#include "main.h"


#ifdef NULL
#undef NULL
#define NULL		0
#endif


// BAND AREA : 917 - 923.5 MHz
// MAX 14 dBM


#define RF_FREQUENCY_MIN                    902300000
#define RF_FREQUENCY_MAX                    914900000
#define FREQUENCY_STEP                         200000


#define TX_OUTPUT_POWER                     20          // dBm  0 ~ 14, MAX: 14
#define LORA_BANDWIDTH                      0           // [0: 125 kHz,
                                                        //  1: 250 kHz,
                                                        //  2: 500 kHz,
                                                        //  3: Reserved]
#define LORA_SPREADING_FACTOR               7           // [SF7..SF12]
#define LORA_CODINGRATE                     1           // [1: 4/5,
                                                        //  2: 4/6,
                                                        //  3: 4/7,
                                                        //  4: 4/8]
#define LORA_PREAMBLE_LENGTH                8           // Same for Tx and Rx   8
#define LORA_SYMBOL_TIMEOUT                 50          // 5 -> 20
#define LORA_FIX_LENGTH_PAYLOAD_ON          false
#define LORA_IQ_INVERSION_ON                false
#define LORA_CRC_ON                         true
#define LORA_TX_TIME_OUT                    100000       // 3000000


#ifdef REGION_US915
// static const uint32_t       BandwidthsUS915[] = { 125000, 125000, 125000, 125000, 500000, 0, 0, 0, 500000, 500000, 500000, 500000, 500000, 500000, 0, 0 };
static const uint8_t        DataratesUS915[]  = { 10, 9, 8,  7,  8,  0,  0, 0, 12, 11, 10, 9, 8, 7, 0, 0 };
static const uint8_t        MaxPayloadOfDatarateRepeaterUS915[] = { 11, 53, 125, 242, 242, 0, 0, 0, 33, 109, 222, 222, 222, 222, 0, 0 };
#endif







void P2P_API_INIT(RadioEvents_t *pEvents, uint8_t u8Channel, uint8_t u8Power, uint8_t u8Rateset)
{
    Radio.Init( pEvents );

    P2P_API_RADIO_INIT(u8Channel, u8Power, u8Rateset);

    HAL_Delay(50);

    P2P_API_RX_ENABLE();
}



void P2P_API_RADIO_INIT(uint8_t u8Channel, uint8_t u8Power, uint8_t u8Rateset)
{
    P2P_API_LORA_RADIO_CHANNEL(u8Channel);
    P2P_API_RADIO_TX_CONFIG(u8Power, u8Rateset);
    P2P_API_RADIO_RX_CONFIG(u8Rateset);
}


void P2P_API_RX_DISABLE(void)
{
    Radio.Sleep();
}


void P2P_API_RX_ENABLE(void)
{
    Radio.Rx(0);
}


uint8_t P2P_API_GET_MAX_POWER(void)
{
    return TX_OUTPUT_POWER;
}


uint32_t P2P_API_GET_MAX_PAYLOAD(uint8_t u8Rateset)
{
    uint32_t maxPayload = 0;
    
    #ifdef REGION_US915
    maxPayload = MaxPayloadOfDatarateRepeaterUS915[u8Rateset];
    #endif
    
    return maxPayload;
}


/* ==========================================================================
**  RS :       SF       Data
**  0  : SF12 /125kHz	 250
**  1  : SF11 /125kHz	 440
**  2  : SF10 /125kHz	 980
**  3  : SF9 /125kHz	1760
**  4  : SF8 /125kHz	3125
**  5  : SF7 /125kHz	5470
** ==========================================================================*/
uint32_t P2P_API_RADIO_TX_CONFIG(uint8_t u8Power, uint8_t u8Rateset)
{

    uint8_t     txpower      = TX_OUTPUT_POWER - u8Power;
    uint32_t    bandwidth    = 0;
    uint32_t    dataRate     = 0;

    #ifdef REGION_US915
    dataRate     = DataratesUS915[u8Rateset];
    if(u8Rateset >= 4) {
        bandwidth = 2;
    }
	#endif

    Radio.SetTxConfig(  MODEM_LORA,	txpower, 0, bandwidth, dataRate,
                        LORA_CODINGRATE, LORA_PREAMBLE_LENGTH, LORA_FIX_LENGTH_PAYLOAD_ON,
                        LORA_CRC_ON, 0, 0, LORA_IQ_INVERSION_ON, LORA_TX_TIME_OUT );

    uint32_t maxPayload = P2P_API_GET_MAX_PAYLOAD(u8Rateset);
    Radio.SetMaxPayloadLength( MODEM_LORA, maxPayload );

    return maxPayload;
}




void P2P_API_RADIO_RX_CONFIG(uint8_t u8Rateset)
{
    uint32_t    bandwidth = 0;
    uint32_t    dataRate = 0;
    
	#ifdef REGION_US915

    dataRate = DataratesUS915[u8Rateset];
    if(u8Rateset >= 4) {
        bandwidth = 2;
    }
	#endif

    Radio.SetRxConfig(MODEM_LORA, bandwidth, dataRate,
                    LORA_CODINGRATE, 0, LORA_PREAMBLE_LENGTH,
                    LORA_SYMBOL_TIMEOUT, LORA_FIX_LENGTH_PAYLOAD_ON,
                    0, LORA_CRC_ON, 0, 0, LORA_IQ_INVERSION_ON, true );

    uint32_t maxPayload = P2P_API_GET_MAX_PAYLOAD(u8Rateset);
    Radio.SetMaxPayloadLength( MODEM_LORA, maxPayload );

}




void P2P_API_LORA_RADIO_CHANNEL(uint8_t u8Channel)
{
    uint32_t channel = RF_FREQUENCY_MAX;
    if(u8Channel > 63) {
        u8Channel = 63;
    }

    channel = (RF_FREQUENCY_MAX - (FREQUENCY_STEP * u8Channel));
    if(channel < RF_FREQUENCY_MIN) {
        channel = RF_FREQUENCY_MIN;
    }
    
    Radio.SetChannel(channel);
}



void P2P_API_SEND(uint8_t* pTxData, int nTxSize)
{
    Radio.Send(pTxData, nTxSize);
}

