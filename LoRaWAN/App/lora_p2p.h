/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __LORA_P2P_H__
#define __LORA_P2P_H__



#include "radio.h"


extern void         P2P_API_INIT(RadioEvents_t *pEvents, uint8_t u8Channel, uint8_t u8Power, uint8_t u8Rateset);
extern void         P2P_API_RADIO_INIT(uint8_t u8Channel, uint8_t u8Power, uint8_t u8Rateset);
extern void         P2P_API_RX_ENABLE(void);
extern void         P2P_API_RX_DISABLE(void);


extern uint8_t      P2P_API_GET_MAX_POWER(void);
extern uint32_t     P2P_API_GET_MAX_PAYLOAD(uint8_t u8Rateset);
extern uint32_t     P2P_API_RADIO_TX_CONFIG(uint8_t u8Power, uint8_t u8Rateset);
extern void         P2P_API_RADIO_RX_CONFIG(uint8_t u8Rateset);
extern void         P2P_API_LORA_RADIO_CHANNEL(uint8_t u8Channel);
extern void         P2P_API_SEND(uint8_t* pTxData, int nTxSize);




#endif /*__LORA_P2P_H__*/

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
