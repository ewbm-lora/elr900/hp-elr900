/**
  ******************************************************************************
  * @file    adc.c
  * @brief   This file provides code for the configuration
  *          of the ADC instances.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under Ultimate Liberty license
  * SLA0044, the "License"; You may not use this file except in compliance with
  * the License. You may obtain a copy of the License at:
  *                             www.st.com/SLA0044
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "adc.h"
#include "string.h"
#include "math.h"
#include "stdio.h"
#include "mw_log_conf.h"


#define ADC_RESOLUTION                          4095        /* 12bit        */
#define REGISTER_R1_VBATT                       1000000      /* 1M Ohm      */
#define REGISTER_R2_VBATT                       1000000      /* 1M Ohm      */
#define COMPANSATION                            0.0008
#define MCU_VCC                                 3.3

#define BRIDGE_RESISTOR							10000		/* 10k	*/






ADC_HandleTypeDef hadc;

/* ADC init function */
void MX_ADC_Init(void)
{
    // ADC_ChannelConfTypeDef sConfig = {0};
    memset(&hadc, 0, sizeof(hadc));
    
#if 0
    hadc.Instance = ADC;
    hadc.Init.OversamplingMode = DISABLE;
    hadc.Init.ClockPrescaler = ADC_CLOCK_SYNC_PCLK_DIV2;
    hadc.Init.Resolution = ADC_RESOLUTION_12B;
    hadc.Init.ScanConvMode = ADC_SCAN_DIRECTION_FORWARD;
    hadc.Init.DataAlign = ADC_DATAALIGN_RIGHT;
    
    hadc.Init.EOCSelection = ADC_EOC_SINGLE_CONV;
    hadc.Init.LowPowerAutoWait = DISABLE;
    hadc.Init.LowPowerAutoPowerOff = DISABLE;
    hadc.Init.ContinuousConvMode = DISABLE;
    hadc.Init.NbrOfConversion = 2;
    hadc.Init.DiscontinuousConvMode = DISABLE;
    hadc.Init.ExternalTrigConv = ADC_SOFTWARE_START;
    hadc.Init.ExternalTrigConvEdge = ADC_EXTERNALTRIGCONVEDGE_NONE;
    hadc.Init.DMAContinuousRequests = DISABLE;
    hadc.Init.Overrun = ADC_OVR_DATA_OVERWRITTEN;
    hadc.Init.SamplingTimeCommon1 = ADC_SAMPLETIME_160CYCLES_5;
    hadc.Init.SamplingTimeCommon2 = ADC_SAMPLETIME_160CYCLES_5;
    
    hadc.Init.TriggerFrequencyMode = ADC_TRIGGER_FREQ_HIGH;
#else
    hadc.Instance = ADC;
    hadc.Init.OversamplingMode = DISABLE;
    hadc.Init.ClockPrescaler = ADC_CLOCK_SYNC_PCLK_DIV2;
    hadc.Init.Resolution = ADC_RESOLUTION_12B;
    hadc.Init.ScanConvMode = ADC_SCAN_DIRECTION_FORWARD;
    hadc.Init.DataAlign = ADC_DATAALIGN_RIGHT;
    hadc.Init.ContinuousConvMode = ENABLE;
    hadc.Init.DiscontinuousConvMode = DISABLE;
    hadc.Init.ExternalTrigConvEdge = ADC_EXTERNALTRIGCONVEDGE_NONE;
    hadc.Init.ExternalTrigConv = ADC_SOFTWARE_START;
    hadc.Init.DMAContinuousRequests = DISABLE;
    hadc.Init.EOCSelection = ADC_EOC_SINGLE_CONV;
    hadc.Init.Overrun = ADC_OVR_DATA_PRESERVED;
    hadc.Init.LowPowerAutoWait = DISABLE;
    hadc.Init.LowPowerAutoPowerOff = DISABLE;
    hadc.Init.NbrOfConversion = 1;
    
#endif

    
    if (HAL_ADC_Init(&hadc) != HAL_OK) {
        Error_Handler();
    }
    

#if 0
    sConfig.Rank = 0;
    sConfig.Channel = ADC_CHANNEL_11;
    
    if (HAL_ADC_ConfigChannel(&hadc, &sConfig) != HAL_OK) {
        Error_Handler();
    }

    sConfig.Rank = 1;
    sConfig.Channel = ADC_CHANNEL_2;
    if (HAL_ADC_ConfigChannel(&hadc, &sConfig) != HAL_OK) {
        Error_Handler();
    }

    if( HAL_ADCEx_Calibration_Start(&hadc) != HAL_OK) {
        Error_Handler( );
    }
#endif

}



void MX_ADC_ReInit(void)
{
    HAL_ADC_Init(&hadc);
}


void HAL_ADC_MspInit(ADC_HandleTypeDef* adcHandle)
{

    GPIO_InitTypeDef GPIO_InitStruct = {0};
    
    if(adcHandle->Instance==ADC) {
        /* ADC clock enable */
        __HAL_RCC_ADC_CLK_ENABLE();
        __HAL_RCC_GPIOA_CLK_ENABLE();
        __HAL_RCC_GPIOB_CLK_ENABLE();

        GPIO_InitStruct.Pin = ADC_TEMP1_IN_Pin;
        GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
        GPIO_InitStruct.Pull = GPIO_NOPULL;
        HAL_GPIO_Init(ADC_TEMP1_IN_GPIO_Port, &GPIO_InitStruct);


        GPIO_InitStruct.Pin = ADC_TEMP2_IN_Pin;
        GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
        GPIO_InitStruct.Pull = GPIO_NOPULL;
        HAL_GPIO_Init(ADC_TEMP2_IN_GPIO_Port, &GPIO_InitStruct);


        /* ADC interrupt Init */
        HAL_NVIC_SetPriority(ADC_IRQn, 0, 0);
        HAL_NVIC_EnableIRQ(ADC_IRQn);
        /* USER CODE BEGIN ADC_MspInit 1 */

        /* USER CODE END ADC_MspInit 1 */
    }
}





void HAL_ADC_MspDeInit(ADC_HandleTypeDef* adcHandle)
{

    if(adcHandle->Instance==ADC) {
        
        __HAL_RCC_ADC_CLK_DISABLE();

        HAL_GPIO_DeInit(ADC_TEMP1_IN_GPIO_Port, ADC_TEMP1_IN_Pin);
        HAL_GPIO_DeInit(ADC_TEMP2_IN_GPIO_Port, ADC_TEMP2_IN_Pin);
        HAL_NVIC_DisableIRQ(ADC_IRQn);
    }
}




void MX_ADC_DeInit(void)
{
    HAL_ADC_MspDeInit(&hadc);

}







unsigned long HW_resistance(uint16_t raw_adc)
{
    float read_adc = (float)raw_adc;
	unsigned long convert_value;	// temporary variable to store calculations in
	convert_value = BRIDGE_RESISTOR/((ADC_RESOLUTION -read_adc)/(read_adc));  //for pull up configuration
	return convert_value; // returns the value calculated to the calling function.

}



float steinharthart(unsigned long resistance)
{

	float temp;	// temporary variable to store calculations in
	float logRes = log(resistance);

	/* calculating logirithms is time consuming for a microcontroller - so we just
	** do this once and store it to a variable.
	** */
	float k0 = 0.8613932737e-3;
	float k1 = 2.563769332e-4;
	float k2 = 1.680552366e-7;

	temp = 1 / (k0 + k1 * logRes + k2 * logRes * logRes * logRes);

	/* convert from Kelvin to Celsius
	*/
	temp = temp - 273.15;

	/* convert to F */
    // temp_f = (temp*9.0)/5.0+32.0;

	return temp;
	
}




float vbatt_calculation(uint16_t adc_raw)
{
	float vbatt;
	vbatt = ((MCU_VCC*adc_raw/ADC_RESOLUTION)*(REGISTER_R2_VBATT+REGISTER_R1_VBATT)/REGISTER_R2_VBATT);

	return vbatt;
}



uint32_t adc_read_channel(uint32_t channel)
{
    uint32_t                    ADCxConvertedValues = 0;
    ADC_ChannelConfTypeDef      sConfig = {0};

    MX_ADC_Init();

    /* Start Calibration */
    if (HAL_ADCEx_Calibration_Start(&hadc) != HAL_OK) {
        Error_Handler();
    }

    /* Configure Regular Channel */
    sConfig.Channel = channel;
    sConfig.Rank = ADC_REGULAR_RANK_1;
    sConfig.SamplingTime = ADC_SAMPLINGTIME_COMMON_1;
    if (HAL_ADC_ConfigChannel(&hadc, &sConfig) != HAL_OK) {
        Error_Handler();
    }

    if (HAL_ADC_Start(&hadc) != HAL_OK) {
        Error_Handler();
    }
    
    /** Wait for end of conversion */
    HAL_ADC_PollForConversion(&hadc, HAL_MAX_DELAY);

    /** Wait for end of conversion */
    HAL_ADC_Stop(&hadc) ;   /* it calls also ADC_Disable() */

    ADCxConvertedValues = HAL_ADC_GetValue(&hadc);

    HAL_ADC_DeInit(&hadc);

    return ADCxConvertedValues;

}



#if 0
bool adc_temp_read(uint16_t *adcValue)
{
    uint16_t            adc_value;
    unsigned long       thermistor;

    // Temp1 Read
    HAL_ADC_Start(&hadc);
    
    HAL_ADC_PollForConversion(&hadc,3000);
    adc_value   = HAL_ADC_GetValue(&hadc);
    thermistor  = HW_resistance(adc_value);
    adcValue[0] = (uint16_t)(steinharthart(thermistor) * 100);

    // Temp2 Read
    HAL_ADC_PollForConversion(&hadc,3000);
    adc_value   = HAL_ADC_GetValue(&hadc);
    thermistor  = HW_resistance(adc_value);
    adcValue[1] = (uint16_t)(steinharthart(thermistor) * 100);

	HAL_ADC_Stop(&hadc);

    // PRINTFD("adc-read : Temp1=%d  Temp2=%d\r\n", (int)adcValue[0], (int)adcValue[1]);

    return true;
	
}
#else
bool adc_temp_read(uint16_t *adcValue)
{
	uint16_t            uValue;
    unsigned long       thermistor;

    uValue      = adc_read_channel(ADC_CHANNEL_11);
    thermistor  = HW_resistance(uValue);
    adcValue[0] = (uint16_t)(steinharthart(thermistor) * 100);

    uValue      = adc_read_channel(ADC_CHANNEL_2);
    thermistor  = HW_resistance(uValue);
    adcValue[1] = (uint16_t)(steinharthart(thermistor) * 100);

    return true;
}

#endif





/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
