/**
  ******************************************************************************
  * @file    lora_app.c
  * @author  MCD Application Team
  * @brief   Application of the LRWAN Middleware
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under Ultimate Liberty license
  * SLA0044, the "License"; You may not use this file except in compliance with
  * the License. You may obtain a copy of the License at:
  *                             www.st.com/SLA0044
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "platform.h"
#include "sys_app.h"
#include "radio.h"
#include "stm32_seq.h"
#include "stm32_timer.h"
#include "utilities_def.h"
#include "lora_command.h"
#include "lora_app.h"
#include "lora_p2p.h"
#include "lora_at.h"
#include "flash.h"
#include "adc.h"
#include "main.h"
#include "stdlib.h"
#include "stdio.h"



#ifdef NULL
#undef NULL
#define NULL		0
#endif




#define TIME_APP_TICK_INT           50
#define RX_TAIL_CHAR                0x1A
#define MAX_SEND_RETRY              3



/* HEADER_CHAR --------------------------*/
#define HDR_C_TO_M_REG_INFO         '1'
#define HDR_C_TO_M_RSP_RPT          '2'

#define HDR_M_TO_C_REQ_PWM          '5'     /* Change PWM */
#define HDR_M_TO_C_REQ_RPT          '6'     /* Report Requested*/
#define HDR_M_TO_C_REQ_NODE         '7'     /* Change Node, Master-ID, And Auto Reset */
#define HDR_M_TO_C_REQ_RST          '8'      /* Device Reset */





#define MASTER_NODE_ID				0







#define CLEAR_EVENT()               (sEvent.eEvent = APP_NONE_E)





static fSetSleepStart               cbSetSleepStart = NULL;
static TimerEvent_t                 appTickTimer;
static TimerEvent_t                 nodeReportTimer;


static sAppInfoType			        APPINFO;
static RadioEvents_t                RadioP2PEvents;

static eAppSateType                 curAppState = APP_ENTER_S;
static eAppSateType                 newAppState;



static uint8_t                      TX_BUFF[128];




static void                         AppStateMechine(sAppEventType sEvent);





extern void                         HW_PWM_Led_SetBrightness(uint8_t nValue);
extern uint32_t                     HW_PWM_Led_GetBrightness(void);







/*=============================================================================
** EVENT
** ============================================================================*/
static sAppEventType EVENT(eAppEventType eEvent, uint32_t uParam1, uint32_t uParam2)
{
    sAppEventType sEvent;
    memset(&sEvent, NULL, sizeof(sAppEventType));

    sEvent.eEvent = eEvent;
    sEvent.PARM.uParam[0] = uParam1;
    sEvent.PARM.uParam[1] = uParam2;

    return sEvent;
}



/*===========================================================================
FUNCTION APP_TICK_TIMEOUT
===========================================================================*/
static bool APP_TICK_TIMEOUT(uint32_t dwTimeout)
{
    uint32_t dwMaxCount = (dwTimeout / TIME_APP_TICK_INT)+1;

    if(APPINFO.RUN.nTickCount >= dwMaxCount) {
        return true;
    }
    
    return false;
}



/*=============================================================================
** AddAppEvent
** ============================================================================*/
static void AddAppEvent(sAppEventType sEvent)
{
    UTILS_ENTER_CRITICAL_SECTION();
    
    if(APPINFO.u8EventIndex < EVENT_QUEUE_COUNT) {
        APPINFO.sEventQueue[APPINFO.u8EventIndex++] = sEvent;
    }
    
    UTILS_EXIT_CRITICAL_SECTION();
}



/*=============================================================================
** CmdProcessNotify
** ============================================================================*/
static void     CmdProcessNotify(void)
{
	/* Call CMD_Process -------- */
    UTIL_SEQ_SetTask((1 << CFG_SEQ_Task_Vcom), 0);

}


/*=============================================================================
** SetAppTickTimer
** ============================================================================*/
static void SetAppTickTimer(uint32_t time)
{
    if(time == 0) {
        TimerStop(&appTickTimer);
    }
    else {
        TimerStop(&appTickTimer);
        TimerSetValue(&appTickTimer, time);
        TimerStart(&appTickTimer);
    }
}



/*=============================================================================
** OnAppTickTimer
** ============================================================================*/
static void	    OnAppTickTimer(void *pUser)
{
	UTILS_ENTER_CRITICAL_SECTION();

    for(uint8_t i=0; i<APPINFO.u8EventIndex; i++) {
    	AppStateMechine(APPINFO.sEventQueue[i]);
    }

    APPINFO.u8EventIndex = 0;
    APPINFO.RUN.nTickCount++;
    
    AppStateMechine(EVENT(APP_TICK_TIMER_E, NULL, NULL));

    UTILS_EXIT_CRITICAL_SECTION();

    SetAppTickTimer(TIME_APP_TICK_INT);
}



/*=============================================================================
** SetRspTimerStart
** ============================================================================*/
static void     SetRspTimerStart(uint32_t time)
{
    int nInterval = time * GetFLASH()->u8NodeId;
    
    TimerStop(&nodeReportTimer);
    TimerSetValue(&nodeReportTimer, nInterval);
    TimerStart(&nodeReportTimer);

    PRINTFD("SetRspTimerStart %d \r\n", time);
}




/*=============================================================================
** OnAppTickTimer
** ============================================================================*/
static void	    OnNodeReportTimer(void *pUser)
{
	TimerStop(&nodeReportTimer);
    AddAppEvent(EVENT(APP_NODE_REPORT_E, 0, 0));

    PRINTFD("OnNodeReportTimer\r\n");
}




/*=============================================================================
** ParseRxData
** Rx Format
**     "Command":
** ============================================================================*/
static int ParseRxData(char *sData, int dataLen, uint8_t *txSeqCnt, uint8_t *srcNode, uint8_t *destNode, uint8_t *hopLevel, uint8_t *rxData, int *rxSize)
{
    uint8_t*    uDataPtr;
    int         nPktSize;

    if(dataLen < 6) {
        return 1;
    }

    uDataPtr = (uint8_t*)&sData[0];
    
    /* Tx Count ---------------------------------------------------------------------------*/
    *txSeqCnt   = uDataPtr[0];
    *srcNode    = uDataPtr[1];
    *destNode   = uDataPtr[2];
    *hopLevel   = uDataPtr[3];
    
    PRINTFD("TxSeq=%d, Src=%d, Dec=%d, Hop=%d\r\n", *txSeqCnt, *srcNode, *destNode, *hopLevel);

    nPktSize = (dataLen - 4);
    if(nPktSize < 2) {
        return 1;
    }
    
    memcpy(rxData, &sData[4], nPktSize);
    if(rxData[nPktSize-1] != RX_TAIL_CHAR) {
        return 2;
    }

    nPktSize--;
    *rxSize = nPktSize;

#if 0
    PRINTFD("RXDTA : Size=%d : ", nPktSize);
    for(int i=0; i<nPktSize; i++) {
        PRINTFD("%02X,", (uint8_t)rxData[i]);
    }
    PRINTFD("\r\n");
#endif

    return 0;

}



/*=============================================================================
** OnTxDone
** ============================================================================*/
static void OnTxDone( void )
{
    AddAppEvent(EVENT(APP_TX_DONE_E, 0, 0));
}




/*=============================================================================
** OnRxDone
** ============================================================================*/
static void OnRxDone(uint8_t *payload, uint16_t size, int16_t rssi, int8_t snr )
{
    uint8_t         uMyNode;
    uint8_t         txSeqCount;
    uint8_t         srcNode;
    uint8_t         destNode;
    uint8_t         hopLevel;
    int             nRxDataSize;
    uint32_t        maxPayload;
    bool            isHopWorking;
    
    sAppEventType   sEvent;

    uMyNode = GetFLASH()->u8NodeId;

    maxPayload = P2P_API_GET_MAX_PAYLOAD(GetFLASH()->u8Rateset);
    
    
    if(size == 0) {
        PRINTFD("OnRxDone Error Code = 0\r\n");
        return;
    }
    
    if(size > maxPayload) {
        PRINTFD("OnRxDone Error Code = 1\r\n");
        return;
	}

    memset(&sEvent, NULL, sizeof(sEvent));
    payload[size] = NULL;

    int nParse = ParseRxData((char*)payload, size, &txSeqCount, &srcNode, &destNode, &hopLevel, (uint8_t*)sEvent.PARM.sData, &nRxDataSize);
    if(nParse != 0) {
        PRINTFD("OnRxDone Error Code = 3\r\n");
        return;
    }

    if(srcNode == uMyNode) {
    	PRINTFD("Sender Skip %d\r\n", srcNode);
		return;

	}

    if(txSeqCount > 0 && APPINFO.LAST_PRE.uSeqCount == txSeqCount) {
        if(APPINFO.LAST_PRE.nDataSize == nRxDataSize && memcmp(APPINFO.LAST_PRE.uDataBuff, sEvent.PARM.sData, nRxDataSize) == 0) {
            PRINTFD("Duplicate rx data %d\r\n", txSeqCount);
            return;
        }
    }

    if(txSeqCount > 0 && APPINFO.LAST_TX.uSeqCount == txSeqCount) {
        if(APPINFO.LAST_TX.nDataSize == nRxDataSize && memcmp(APPINFO.LAST_TX.uDataBuff, sEvent.PARM.sData, nRxDataSize) == 0) {
            PRINTFD("Duplicate rx data %d\r\n", txSeqCount);
            return;
        }
    }

    APPINFO.LAST_PRE.uSeqCount = txSeqCount;
    APPINFO.LAST_PRE.nDataSize = nRxDataSize;
    memcpy(APPINFO.LAST_PRE.uDataBuff, sEvent.PARM.sData, nRxDataSize);

    isHopWorking = false;

    if((destNode == uMyNode) || (destNode == 0xFF)) {
        sEvent.eEvent = APP_RX_DONE_E;
        AddAppEvent(sEvent);

        APPINFO.LAST_RX.uSeqCount = txSeqCount;
        APPINFO.LAST_RX.uSrcNode = srcNode;
        APPINFO.LAST_RX.nRssi = rssi;
        APPINFO.LAST_RX.nDataSize = nRxDataSize;
        
        memcpy(APPINFO.LAST_RX.uDataBuff, sEvent.PARM.sData, nRxDataSize);
        APPINFO.LAST_RX.uDataBuff[nRxDataSize] = NULL;

        if(destNode == 0xFF && hopLevel > 0) {
            isHopWorking = true;
        }
    }
    else {
        if(hopLevel > 0 && uMyNode != 0) {
            isHopWorking = true;
        }
        else {
            PRINTFD("SKIP DATA\r\n");
        }
    }

    if(isHopWorking == true) {
        sEvent.eEvent = APP_NODE_HOP_REQ_E;
        sEvent.PARM.uParam[0] = nRxDataSize;
        sEvent.PARM.uParam[1] = txSeqCount;
        sEvent.PARM.uParam[2] = srcNode;
        sEvent.PARM.uParam[3] = destNode;
        sEvent.PARM.uParam[4] = hopLevel-1;
        AddAppEvent(sEvent);
    }
    
}


/*=============================================================================
** OnTxTimeout
** ============================================================================*/
static void OnTxTimeout( void )
{
    // Radio.Sleep( );
    PRINTFD("OnTxTimeout\r\n");
    AddAppEvent(EVENT(APP_TX_ERR_E, NULL, NULL));	
}



/*=============================================================================
** OnRxTimeout
** ============================================================================*/
static void OnRxTimeout( void )
{
    PRINTFD("OnRxTimeout\r\n");
}



/*=============================================================================
** OnRxError
** ============================================================================*/
static void OnRxError( void )
{
    PRINTFD("OnRxError\r\n");
    AddAppEvent(EVENT(APP_RX_ERR_E, NULL, NULL));	
}


/*=============================================================================
** OnATComChannel
** ============================================================================*/
static ATError_t OnATComChannel(uint8_t channel)
{
    if(curAppState != APP_IDLING_S) {
        return AT_BUSY_ERROR;
    }
    
    GetFLASH()->u8Channel = (uint8_t)channel;
    FLASH_EFS_Write();
    
    AddAppEvent(EVENT(APP_RE_INIT_P2P_E, NULL, NULL));	
    
    return AT_OK;
}



/*=============================================================================
** OnATComRateset
** ============================================================================*/
static ATError_t OnATComRateset(uint8_t rateset)
{
    if(curAppState != APP_IDLING_S) {
        return AT_BUSY_ERROR;
    }
    
    GetFLASH()->u8Rateset = rateset;
    FLASH_EFS_Write();
    
    AddAppEvent(EVENT(APP_RE_INIT_P2P_E, NULL, NULL));	
    
    return AT_OK;
}



/*=============================================================================
** OnAtComRFPower
** ============================================================================*/
static ATError_t OnAtComRFPower(uint8_t power)
{
    if(curAppState != APP_IDLING_S) {
        return AT_BUSY_ERROR;
    }
    
    GetFLASH()->u8Power = power;
    FLASH_EFS_Write();
    
    AddAppEvent(EVENT(APP_RE_INIT_P2P_E, NULL, NULL));	
    
    return AT_OK;
}



/*=============================================================================
** OnATComReset
** ============================================================================*/
static ATError_t OnATComReset(void)
{
    if(curAppState != APP_IDLING_S) {
        return AT_BUSY_ERROR;
    }

    PRINTFD("OnATComReset \r\n");
    AddAppEvent(EVENT(APP_RESET_E, 0, 0));
    return AT_OK;
}


/*=============================================================================
** OnATComFSet
** ============================================================================*/
static ATError_t OnATComFSet(void)
{
    if(curAppState != APP_IDLING_S) {
        return AT_BUSY_ERROR;
    }
    AddAppEvent(EVENT(APP_FSET_E, 0, 0));
    return AT_OK;
}


/*=============================================================================
** OnATComSend
** ============================================================================*/
static ATError_t OnATComSend(uint8_t destNode, uint8_t len, char* tx_data)
{
    sAppEventType   sEvent;
    
    if(curAppState != APP_IDLING_S) {
        return AT_BUSY_ERROR;
    }

    sEvent.eEvent = APP_NODE_SEND_REQ_E;
    memcpy(sEvent.PARM.sData, tx_data, len);
    sEvent.PARM.uParam[0] = len;
    sEvent.PARM.uParam[1] = destNode;

    AddAppEvent(sEvent);
    return AT_OK;

}


/*=============================================================================
** OnATComRxData
** ============================================================================*/
static ATError_t OnATComRxData(void)
{
    char sRxToken[10];
    char sRxBuff[512];
    
    if(APPINFO.LAST_RX.nDataSize == 0) {
        return AT_ERROR;
    }
    
    PRINTF("LASTRX:%d,%d,%d,", APPINFO.LAST_RX.uSrcNode, APPINFO.LAST_RX.nRssi, APPINFO.LAST_RX.nDataSize);

    memset(sRxBuff, NULL, sizeof(sRxBuff));
    for(int i=0; i<APPINFO.LAST_RX.nDataSize; i++) {
        sprintf(sRxToken, "%02X,", APPINFO.LAST_RX.uDataBuff[i]);
        strcat(sRxBuff, sRxToken);
    }

    strcat(sRxBuff, "\r\n");
    PRINTF(sRxBuff);
    return AT_OK;

}




/*=============================================================================
** SendNodeUserData
** ============================================================================*/
static bool SendNodeUserData(void)
{
    int         nPktSize = 0;
    uint32_t    maxSize;

    APPINFO.LAST_TX.nDataSize = APPINFO.RUN.PARM.uParam[0];
    APPINFO.LAST_TX.uSeqCount = APPINFO.u8TxSeqCount;
    memcpy(APPINFO.LAST_TX.uDataBuff, APPINFO.RUN.PARM.sData, APPINFO.LAST_TX.nDataSize);

    TX_BUFF[0] = APPINFO.u8TxSeqCount;
    TX_BUFF[1] = GetFLASH()->u8NodeId;
    TX_BUFF[2] = APPINFO.RUN.PARM.uParam[1];  // dest node id
    TX_BUFF[3] = GetFLASH()->u8HopLevel;

    memcpy(&TX_BUFF[4], APPINFO.RUN.PARM.sData, APPINFO.LAST_TX.nDataSize);

    nPktSize = (4 + APPINFO.LAST_TX.nDataSize);
    TX_BUFF[ nPktSize++ ] = RX_TAIL_CHAR;
    
    maxSize = P2P_API_GET_MAX_PAYLOAD(GetFLASH()->u8Rateset);
    if(maxSize < nPktSize) {
        PRINTFD("MAX TX SIZE ERR (%d/%d  %d)\r\n", nPktSize, maxSize, GetFLASH()->u8Rateset);
        return false;
    }
  
    P2P_API_RADIO_TX_CONFIG(GetFLASH()->u8Power, GetFLASH()->u8Rateset);
    P2P_API_SEND(TX_BUFF, nPktSize);
        
    return true;
}



/*=============================================================================
** SendNodeHopData
** ============================================================================*/
static bool SendNodeHopData(void)
{
    int         nPktSize = 0;
    uint32_t    maxSize;

    APPINFO.LAST_TX.nDataSize = APPINFO.RUN.PARM.uParam[0];
    APPINFO.LAST_TX.uSeqCount = APPINFO.RUN.PARM.uParam[1];
    memcpy(APPINFO.LAST_TX.uDataBuff, APPINFO.RUN.PARM.sData, APPINFO.LAST_TX.nDataSize);
    
    TX_BUFF[0] = APPINFO.RUN.PARM.uParam[1];    // tx seq
    TX_BUFF[1] = APPINFO.RUN.PARM.uParam[2];    // node id
    TX_BUFF[2] = APPINFO.RUN.PARM.uParam[3];    // dest node id
    TX_BUFF[3] = APPINFO.RUN.PARM.uParam[4];    // Hop level

    memcpy(&TX_BUFF[4], APPINFO.RUN.PARM.sData, APPINFO.LAST_TX.nDataSize);
    nPktSize = (4 + APPINFO.LAST_TX.nDataSize);
    
    TX_BUFF[ nPktSize++ ] = RX_TAIL_CHAR;

    
    maxSize = P2P_API_GET_MAX_PAYLOAD(GetFLASH()->u8Rateset);
    if(maxSize < nPktSize) {
        return false;
    }
   
    P2P_API_RADIO_TX_CONFIG(GetFLASH()->u8Power, GetFLASH()->u8Rateset);
    
    P2P_API_SEND(TX_BUFF, nPktSize);
        
    return true;
}




/*=============================================================================
** ReadSenseData
** ============================================================================*/
static uint8_t ReadSenseData(char *pBuffer)
{
    bool        bValue;
    uint8_t     uValue = 0;
    uint16_t    adcValue[2];


    /* Read Temp ----------------------------------------*/
    bValue = adc_temp_read(adcValue);
    if(bValue == false) {
        PRINTF("ADC Temp Read Fail\r\n");
        return 0;
    }

    pBuffer[uValue++] = LORA_APP_VERSION;
   
    memcpy(&pBuffer[uValue], &adcValue[0], 2);
    uValue += 2;
    
    memcpy(&pBuffer[uValue], &adcValue[1], 2);
    uValue += 2;

    pBuffer[uValue++] = (uint8_t)HW_PWM_Led_GetBrightness();

    pBuffer[uValue++] = GetFLASH()->u8Channel;

    memcpy(&pBuffer[uValue], GetFLASH()->uDevEui, 8);
    uValue += 8;

    return uValue;
    
}



/*=============================================================================
** SetupTxClientReg
** ============================================================================*/
static void SetupTxClientReg(void)
{
    uint8_t     uValue = 0;

    APPINFO.RUN.PARM.sData[uValue++] = HDR_C_TO_M_REG_INFO;
    uValue += ReadSenseData(&APPINFO.RUN.PARM.sData[uValue]);

    /* Data Length */
    APPINFO.RUN.PARM.uParam[0] = uValue;

    // Dest Address, Reg data only master zero !!!
    APPINFO.RUN.PARM.uParam[1] = MASTER_NODE_ID;

}



/*=============================================================================
** SetupTxClientReport
** ============================================================================*/
static void SetupTxClientReport(void)
{
    uint8_t     uValue = 0;

    APPINFO.RUN.PARM.sData[uValue++] = HDR_C_TO_M_RSP_RPT;
    uValue += ReadSenseData(&APPINFO.RUN.PARM.sData[uValue]);

    /* Data Length */
    APPINFO.RUN.PARM.uParam[0] = uValue;

    // Dest Address
    APPINFO.RUN.PARM.uParam[1] = MASTER_NODE_ID;
    
}


static void GetHexToString(uint8_t *src, uint8_t *desc, int len)
{
    uint8_t         token;
    
    for(int i=0; i<len; i++) {
        token = src[i*2+0];
        if(token >= '0' && token <= '9') {
            desc[i] = (token - '0');
        }
        else if(token >= 'A' && token <= 'F') {
            desc[i] = (token - 'A' + 10);
        }
        else {
            desc[i] = (token - 'a' + 10);
        }

        desc[i] = desc[i] << 4;

        token = src[i*2+1];
        if(token >= '0' && token <= '9') {
            desc[i] |= (token - '0');
        }
        else if(token >= 'A' && token <= 'F') {
            desc[i] |= (token - 'A' + 10);
        }
        else {
            desc[i] |= (token - 'a' + 10);
        }

    }
}



/*=============================================================================
** APP_Init
** ============================================================================*/
void APP_Init(fSetSleepStart fStart)
{
    cbSetSleepStart = fStart;
    CMD_Init(CmdProcessNotify);

    /*Set verbose LEVEL*/
    UTIL_ADV_TRACE_SetVerboseLevel(GetFLASH()->uDbgLevel);
    UTIL_SEQ_RegTask((1 << CFG_SEQ_Task_Vcom), UTIL_SEQ_RFU, CMD_Process);

    PRINTF("\r\n");
    PRINTF("F/W VERSION %03d\r\n", LORA_APP_VERSION);
    PRINTF("Linemac P2P Soluction\r\n");
    PRINTF("Altorancns Service\r\n");

    PRINTF("NODEID = %d\r\n", GetFLASH()->u8NodeId);
    PRINTF("CHANNEL = %d\r\n", GetFLASH()->u8Channel);
    PRINTF("RATESET = %d\r\n", GetFLASH()->u8Rateset);
    PRINTF("RFPOWER = %d\r\n", GetFLASH()->u8Power);
    PRINTF("DEVEUI = "); print_8_02x(GetFLASH()->uDevEui);

    PRINTF("Compile date = %s\r\n", __DATE__);
    PRINTF("Boot completed\r\n\r\n");

    
    memset(&APPINFO, NULL, sizeof(APPINFO));

    APPINFO.CB.SetChannel       = OnATComChannel;
    APPINFO.CB.SetRateset       = OnATComRateset;
    APPINFO.CB.SetReset         = OnATComReset;
    APPINFO.CB.SetRFPower       = OnAtComRFPower;
    APPINFO.CB.SetFSet          = OnATComFSet;
    APPINFO.CB.SetSend          = OnATComSend;
    APPINFO.CB.GetRxData        = OnATComRxData;


    RadioP2PEvents.TxDone       = OnTxDone;
    RadioP2PEvents.RxDone       = OnRxDone;
    RadioP2PEvents.TxTimeout    = OnTxTimeout;
    RadioP2PEvents.RxTimeout    = OnRxTimeout;
    RadioP2PEvents.RxError      = OnRxError;

    P2P_API_INIT(&RadioP2PEvents, GetFLASH()->u8Channel, GetFLASH()->u8Power, GetFLASH()->u8Rateset);

    TimerInit( &appTickTimer, OnAppTickTimer);
    TimerInit( &nodeReportTimer, OnNodeReportTimer);
    
    SetAppTickTimer(TIME_APP_TICK_INT);

    SetATCommandInfo((void*)&APPINFO);

}





/*=============================================================================
** AppStateMechine
** ============================================================================*/
static void AppStateMechine(sAppEventType sEvent)
{
    bool        bValue;
    uint8_t     uValue;
    int         nValue = 0;
    
    newAppState = curAppState;

    while(newAppState != APP_NONE_S) {


        if(curAppState != newAppState) {

        	if(newAppState == APP_IDLING_S) {

        		/* ------------------------------------------
				 * Tick timer stop !!!, For rx performance
				 * -----------------------------------------*/
				SetAppTickTimer(0);
        	}
        	else if(curAppState == APP_IDLING_S) {

        		/* ------------------------------------------
				 * Restart app tick timer
				 * -----------------------------------------*/
        		SetAppTickTimer(TIME_APP_TICK_INT);
        	}

            // PRINTFD("AppStateMechine Cur=%d,  New=%d  Event=%d]\r\n", curAppState, newAppState, sEvent.eEvent);
        }

        curAppState = newAppState;
        newAppState = APP_NONE_S;


        switch(curAppState) {
            case APP_ENTER_S :

                // Master ID
                if(GetFLASH()->u8NodeId == 0) {
                    newAppState = APP_IDLE_INIT_S;
                }
                else {
                    HW_PWM_Led_SetBrightness(98);
                    newAppState = APP_CLIENT_INIT_S;
                }
                break;


            case APP_CLIENT_INIT_S :
                
                /* Master 0 : Reg Channel ----------------*/
                P2P_API_RX_DISABLE();
                P2P_API_RADIO_INIT(MASTER_NODE_ID, GetFLASH()->u8Power, GetFLASH()->u8Rateset);
                P2P_API_RX_ENABLE();

                /* Client Reg Tx Data Setup ---------*/
                SetupTxClientReg();
                newAppState = APP_NODE_SEND_REQ_S;
                CLEAR_EVENT();
                break;

                

            case APP_IDLE_INIT_S :
                
                P2P_API_RX_DISABLE();
                P2P_API_RADIO_INIT(GetFLASH()->u8Channel, GetFLASH()->u8Power, GetFLASH()->u8Rateset);
                P2P_API_RX_ENABLE();

                APPINFO.nRetry = 0;
                newAppState = APP_IDLING_S;

                CLEAR_EVENT();
                break;


            case APP_IDLING_S :
                switch(sEvent.eEvent) {
                    case APP_NODE_SEND_REQ_E :
                        APPINFO.nRetry = 0;
                        APPINFO.RUN.PARM = sEvent.PARM;
                        newAppState = APP_NODE_SEND_REQ_S;
                        CLEAR_EVENT();
                        break;


                    case APP_NODE_HOP_REQ_E :
                        APPINFO.nRetry = 0;
                        APPINFO.RUN.PARM = sEvent.PARM;
                        newAppState = APP_NODE_HOP_REQ_S;
                        CLEAR_EVENT();
                        break;


                    case APP_RX_DONE_E :
                        CLEAR_EVENT();
                        if(APPINFO.LAST_RX.nDataSize == 0) {
                            break;
                        }
                        
                        OnATComRxData();

                        /* Master Tx Data Handler -----------------------------------------*/
                        if(GetFLASH()->u8NodeId > 0 && APPINFO.LAST_RX.uSrcNode == 0) {

                            /* Requested from master, device temp and pwm valus -----------*/
                            if(APPINFO.LAST_RX.uDataBuff[0] == HDR_M_TO_C_REQ_PWM) {

                                uValue = (APPINFO.LAST_RX.uDataBuff[1] - '0') * 10;
                                uValue += (APPINFO.LAST_RX.uDataBuff[2] - '0');
                            
                                PRINTFD("SetBrightness %d\r\n", uValue);
                                HW_PWM_Led_SetBrightness(uValue);
                            }
                            else {

                                GetHexToString(&APPINFO.LAST_RX.uDataBuff[1], APPINFO.uTempBuf, 8);
                                nValue = memcmp(GetFLASH()->uDevEui, APPINFO.uTempBuf, 8);
                                if(nValue != 0) {
                                    PRINTFD("Skip Eui : "); print_8_02x(APPINFO.uTempBuf);
                                }
                                else {
                                    if(APPINFO.LAST_RX.uDataBuff[0] == HDR_M_TO_C_REQ_RPT) {
                                        SetRspTimerStart(100);
                                    }
                                    else if(APPINFO.LAST_RX.uDataBuff[0] == HDR_M_TO_C_REQ_RST) {
                                        APPINFO.RUN.nTickCount = 0;
                                        newAppState = APP_RESET_S;
                                        break;
                                    }
                                    else if(APPINFO.LAST_RX.uDataBuff[0] == HDR_M_TO_C_REQ_NODE) {
                                        
                                        uValue = (APPINFO.LAST_RX.uDataBuff[1+16+0] - '0') * 10;
                                        uValue += (APPINFO.LAST_RX.uDataBuff[1+16+1] - '0');
                                        PRINTFD("Channel Change : %d\r\n", uValue);
                                        GetFLASH()->u8Channel = uValue;

                                        uValue = (APPINFO.LAST_RX.uDataBuff[1+16+2] - '0') * 10;
                                        uValue += (APPINFO.LAST_RX.uDataBuff[1+16+3] - '0');
                                        PRINTFD("Node Change : %d\r\n", uValue);
                                        GetFLASH()->u8NodeId = uValue ;

                                        FLASH_EFS_Write();
                                        
                                        HAL_Delay(500);

                                        APPINFO.RUN.nTickCount = 0;
                                        newAppState = APP_RESET_S;
                                        break;
                                    }
                                }
                                
                            }
                            
                            
                        }

                        newAppState = APP_IDLE_INIT_S;
                        break;
                        

                    case APP_NODE_REPORT_E :
                        SetupTxClientReport();
                        newAppState = APP_NODE_SEND_REQ_S;
                        break;


                    case APP_TX_DONE_E :
                    case APP_RX_ERR_E :
                    case APP_TX_ERR_E :
                    case APP_RE_INIT_P2P_E :
                        newAppState = APP_IDLE_INIT_S;
                        break;


                    default :
                    	break;
                }
                break;


             case APP_NODE_SEND_REQ_S :
                if(APPINFO.nRetry >= MAX_SEND_RETRY) {
                    newAppState = APP_IDLE_INIT_S;
                    break;
                }

                /* 0 : Length
                ** 1 : Dec node id 
                */
                if(APPINFO.RUN.PARM.uParam[1] == GetFLASH()->u8NodeId) {
                    PRINTF("NODE ID DEST ERROR\r\n");
                    newAppState = APP_IDLE_INIT_S;
                    break;
                }

                P2P_API_RX_DISABLE();

                APPINFO.RUN.nTickCount = 0;
                
                if(SendNodeUserData() == false) {
                    newAppState = APP_IDLE_INIT_S;
                    break;
                }
                newAppState = APP_NODE_SEND_RSP_S;
                CLEAR_EVENT();
                break;



            case APP_NODE_SEND_RSP_S :
                switch(sEvent.eEvent) {
                    case APP_TICK_TIMER_E :
                        if(APP_TICK_TIMEOUT(5000)) {
                            APPINFO.nRetry++;
                            newAppState = APP_NODE_SEND_REQ_S;
                            PRINTF("TX SEND: TIMEOUT (%d/%d)\r\n", APPINFO.nRetry, MAX_SEND_RETRY);
                            break;
                        }
                        CLEAR_EVENT();
                        break;
                        
                
                    case APP_TX_DONE_E :
                        P2P_API_RX_ENABLE();
                        
                        if(APPINFO.u8TxSeqCount++ >= 128) {
                            APPINFO.u8TxSeqCount = 1;
                        }

                        PRINTFD("TX DONE %d\r\n", APPINFO.u8TxSeqCount);
                        
                        newAppState = APP_IDLE_INIT_S;
                        CLEAR_EVENT();
                        break;


                    case APP_TX_ERR_E :
                    case APP_RX_ERR_E :
                        APPINFO.nRetry++;
                        newAppState = APP_NODE_SEND_REQ_S;
                        PRINTF("TX SEND: ERROR (%d/%d)\r\n", APPINFO.nRetry, MAX_SEND_RETRY);
                        break;


                    default :
                        break;
                }
                break;



            case APP_NODE_HOP_REQ_S :
                switch(sEvent.eEvent) {
                    case APP_TICK_TIMER_E :
                        bValue = false;
                        if(GetFLASH()->u8HopDelay == 0) {
                            bValue = true;
                        }
                        else if(APP_TICK_TIMEOUT(GetFLASH()->u8HopDelay*1000)) {
                            bValue = true;
                        }

                        if(bValue == true) {
                            
                            P2P_API_RX_DISABLE();
                            SendNodeHopData();

                            APPINFO.RUN.nTickCount = 0;
                            newAppState = APP_NODE_HOP_RSP_S;
                        }
                        CLEAR_EVENT();
                        break;

                    default :
                        break;
                }
                break;
                
                

            case APP_NODE_HOP_RSP_S :
                switch(sEvent.eEvent) {
                    case APP_TICK_TIMER_E :
                        if(APP_TICK_TIMEOUT(5000)) {
                            newAppState = APP_IDLE_INIT_S;
                            PRINTF("HOP SEND: TIMEOUT\r\n");
                            break;
                        }
                        CLEAR_EVENT();
                        break;
                        
                
                    case APP_TX_DONE_E :
                        PRINTFD("HOP DONE\r\n");
                        newAppState = APP_IDLE_INIT_S;
                        CLEAR_EVENT();
                        break;


                    case APP_TX_ERR_E :
                    case APP_RX_ERR_E :
                        newAppState = APP_IDLE_INIT_S;
                        PRINTF("HOP SEND: TIMEOUT\r\n");
                        break;


                    default :
                        break;
                }
                break;


            case APP_RESET_S :
                switch(sEvent.eEvent) {
                    case APP_TICK_TIMER_E :
                        if(APP_TICK_TIMEOUT(500)) {
                            NVIC_SystemReset();
                            newAppState = APP_EXIT_S;
                            break;
                        }
                        CLEAR_EVENT();
                        break;
                        

                    default :
                        break;
                }
                break;


            default :
                break;
        }
    }
    
}



