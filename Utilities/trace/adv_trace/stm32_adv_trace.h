/**
 ******************************************************************************
 * @file    stm32_adv_trace.h
 * @author  MCD Application Team
 * @brief   Header for stm32_adv_trace.c
******************************************************************************
  * @attention
  *
 * <h2><center>&copy; Copyright (c) 2019 STMicroelectronics.
 * All rights reserved.</center></h2>
 *
 * This software component is licensed by ST under BSD 3-Clause license,
 * the "License"; You may not use this file except in compliance with the
 * License. You may obtain a copy of the License at:
 *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
*/

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __ADV_TRACE_H
#define __ADV_TRACE_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stdint.h"
#include "utilities_conf.h"

/** @defgroup ADV_TRACE advanced tracer
  * @{
  */

/* Exported types ------------------------------------------------------------*/
/** @defgroup ADV_TRACE_exported_TypeDef ADV_TRACE exported Typedef
 *  @{
 */

/**
 *  @brief prototype of the time stamp function.
 */
typedef void cb_timestamp(uint8_t *pData, uint16_t *Size);

/**
 *  @brief prototype of the overrun function.
 */
typedef void cb_overrun(uint8_t **pData, uint16_t *size);
/**
 *  @brief  List the Advanced trace function status.
 *  list of the returned status value, any negative value is corresponding to an error.
 */
typedef enum{
  UTIL_ADV_TRACE_OK              =  0,     /*!< Operation terminated successfully.*/
  UTIL_ADV_TRACE_INVALID_PARAM   = -1,     /*!< Invalid Parameter.                */
  UTIL_ADV_TRACE_HW_ERROR        = -2,     /*!< Hardware Error.                   */
  UTIL_ADV_TRACE_MEM_FULL        = -3,     /*!< Memory fifo full.                 */
  UTIL_ADV_TRACE_UNKNOWN_ERROR   = -4,     /*!< Unknown Error.                    */
#if defined(UTIL_ADV_TRACE_CONDITIONNAL)
  UTIL_ADV_TRACE_GIVEUP          = -5,     /*!< trace give up                     */
  UTIL_ADV_TRACE_REGIONMASKED    = -6      /*!< trace region masked               */
#endif
} UTIL_ADV_TRACE_Status_t;

/**
 * @brief Advanced trace driver definition
 */
typedef struct {
  UTIL_ADV_TRACE_Status_t  (* Init)(void (*cb)(void *ptr));                                       /*!< Media initialization.      */
  UTIL_ADV_TRACE_Status_t  (* DeInit)(void);                                                      /*!< Media Un-initialization.   */
  UTIL_ADV_TRACE_Status_t  (* StartRx)(void (*cb)(uint8_t *pdata, uint16_t size, uint8_t error)); /*!< Media to start RX process. */
  UTIL_ADV_TRACE_Status_t  (* Send)(uint8_t *pdata, uint16_t size);                               /*!< Media to send data.        */
}UTIL_ADV_TRACE_Driver_s;

/**
 *  @}
 */

/* External variables --------------------------------------------------------*/
/** @defgroup ADV_TRACE_exported_variables ADV_TRACE exported variables
 *
 *  @{
 */
/**
 *  @brief This structure is the linked with the IF layer implementation.
 */
extern const UTIL_ADV_TRACE_Driver_s UTIL_TraceDriver;

/**
 *  @}
 */

/* Exported constants --------------------------------------------------------*/
/* Exported macros -----------------------------------------------------------*/
/* Exported functions ------------------------------------------------------- */
/** @defgroup ADV_TRACE_exported_function ADV_TRACE exported function
 *  @{
 */

/**
 * @brief TraceInit Initializes Logging feature
 * @retval Status based on @ref UTIL_ADV_TRACE_Status_t
 */
UTIL_ADV_TRACE_Status_t UTIL_ADV_TRACE_Init(void);

/**
 * @brief TraceDeInit module DeInitializes.
 * @retval Status based on @ref UTIL_ADV_TRACE_Status_t
 */
UTIL_ADV_TRACE_Status_t UTIL_ADV_TRACE_DeInit(void);

/**
 * @brief this function check if the buffer is empty.
 * @retval 1 if the buffer is empty else 0
 */
uint8_t UTIL_ADV_TRACE_IsBufferEmpty(void);

/**
 * @brief start the RX process.
 * @param UserCallback ptr function used to get the RX data
 * @retval Status based on @ref UTIL_ADV_TRACE_Status_t
 */
UTIL_ADV_TRACE_Status_t UTIL_ADV_TRACE_StartRxProcess(void (*UserCallback)(uint8_t *PData, uint16_t Size, uint8_t Error));


/**
 * @brief  Trace send pre hook function
 */
void UTIL_ADV_TRACE_PreSendHook(void);

/**
 * @brief  Trace send post hook function
 */
void UTIL_ADV_TRACE_PostSendHook(void);




/**
 * @brief conditional FSend decode the strFormat and post it to the circular queue for printing
 * @param VerboseLevel verbose level of the trace
 * @param Region region of the trace
 * @param TimeStampState 0 no time stamp insertion, 1 time stamp inserted inside the trace data
 * @param strFormat formatted string
 * @retval Status based on @ref UTIL_ADV_TRACE_Status_t
 */
UTIL_ADV_TRACE_Status_t UTIL_ADV_TRACE_FSend(uint32_t VerboseLevel, const char *strFormat, ...);



/**
 * @brief  Set the verbose level
 * @param  Level (0 to 255)
 * @retval None
 */
void UTIL_ADV_TRACE_SetVerboseLevel(uint8_t Level);

/**
 * @brief  Get the verbose level
 * @retval verbose level
 */
uint8_t UTIL_ADV_TRACE_GetVerboseLevel(void);




/**
  * @}
  */

/**
  * @}
  */

#ifdef __cplusplus
}
#endif

#endif /*__ADV_TRACE_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
