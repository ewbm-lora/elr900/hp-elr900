/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file    test_rf.c
  * @author  MCD Application Team
  * @brief   manages tx tests
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under Ultimate Liberty license
  * SLA0044, the "License"; You may not use this file except in compliance with
  * the License. You may obtain a copy of the License at:
  *                             www.st.com/SLA0044
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "platform.h"
#include "sys_app.h"
#include "test_rf.h"
#include "radio.h"
#include "stm32_seq.h"
#include "utilities_def.h"
#include "loramac.h"
#include "mw_log_conf.h"

/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* External variables ---------------------------------------------------------*/
/* USER CODE BEGIN EV */

/* USER CODE END EV */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
#define F_868MHz                      868000000
#define F_9221MHz                     922100000

#define P_14dBm                       14
#define P_22dBm                       22
#define SF12                          12
#define CR4o5                         1
#define EMISSION_POWER                P_14dBm
#define CONTINUOUS_TIMEOUT           0xFFFF
#define LORA_PREAMBLE_LENGTH          8         /* Same for Tx and Rx */
#define LORA_SYMBOL_TIMEOUT           30        /* Symbols */
#define TX_TIMEOUT_VALUE              3000
#define LORA_FIX_LENGTH_PAYLOAD_OFF   false
#define LORA_IQ_INVERSION_OFF         false
#define TX_TEST_TONE                  (1<<0)
#define RX_TEST_RSSI                  (1<<1)
#define TX_TEST_LORA                  (1<<2)
#define RX_TEST_LORA                  (1<<3)
#define RX_TIMEOUT_VALUE              5000
#define RX_CONTINUOUS_ON              1
#define PRBS9_INIT                    ( ( uint16_t) 2 )
#define DEFAULT_PAYLOAD_LEN           16
#define DEFAULT_LDR_OPT               2
#define DEFAULT_FSK_DEVIATION         25000
#define DEFAULT_GAUSS_BT              3 /*Lora default in legacy*/

/* USER CODE BEGIN PD */

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
static uint8_t TestState = 0;

static testParameter_t testParam = { TEST_LORA, F_9221MHz, EMISSION_POWER, BW_125kHz, SF12, CR4o5, 0, 0, DEFAULT_PAYLOAD_LEN, DEFAULT_FSK_DEVIATION, DEFAULT_LDR_OPT, DEFAULT_GAUSS_BT};

static __IO uint32_t RadioTxDone_flag = 0;
static __IO uint32_t RadioTxTimeout_flag = 0;
static __IO uint32_t RadioRxDone_flag = 0;
static __IO uint32_t RadioRxTimeout_flag = 0;
static __IO uint32_t RadioError_flag = 0;
static __IO int16_t last_rx_rssi = 0;
static __IO int8_t last_rx_LoraSnr_FskCfo = 0;



#define MAX_RECEIVED_DATA                           255
#define KR920_RSSI_FREE_TH                          -65
#define KR920_CARRIER_SENSE_TIME                    6

#define LORA_SPREADING_FACTOR                       7        // [SF7..SF12]
#define LORA_CODINGRATE                             1         // [1: 4/5,


#define FSK_FDEV                                    25000     // Hz
#define FSK_DATARATE                                50000     // bps
#define FSK_BANDWIDTH                               50000     // Hz
#define FSK_AFC_BANDWIDTH                           83333     // Hz
#define FSK_PREAMBLE_LENGTH                         5         // Same for Tx and Rx
#define FSK_FIX_LENGTH_PAYLOAD_ON                   false



typedef enum {
    TM_NORMAL = 0,
    TM_LBT, // Listen Before Talk
    TM_DC, // Duty Cycle
}TestMode_t;

LoRaMacRegion_t     TestRF_Region = LORAMAC_REGION_KR920;
static bool         TestRF_Active = false;
static uint32_t     TestRF_TxFrequency;
static TestMode_t   TestRF_tm = TM_NORMAL;
static char         TestRF_RxData[MAX_RECEIVED_DATA];
static uint32_t     TestRF_TxSize;
static uint32_t     TestRF_TxTimerPeriod;
static int          TestRF_LbtRssiFreeTh;
static int          TestRF_LbtCarrierSenseTime;
static uint32_t     TestRF_LbtSendDelay;
static uint32_t     TestRF_TxTimeOnAir; // time on AIR (ms)
static TimerEvent_t TestRF_TxTimer;
static TimerEvent_t TestRF_RxTxTimer;





/*!
 * Radio events function pointer
 */
static RadioEvents_t RadioEvents;

/*!
 * Radio test payload pointer
 */
static uint8_t payload[256] = {0};

/* USER CODE BEGIN PV */

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/

/*!
 * \brief Generates a PRBS9 sequence
 */
static int32_t Prbs9_generator(uint8_t *payload, uint8_t len);
/*!
 * \brief Function to be executed on Radio Tx Done event
 */
void OnTxDone(void);

/*!
 * \brief Function to be executed on Radio Rx Done event
 */
void OnRxDone(uint8_t *payload, uint16_t size, int16_t rssi, int8_t snr);

/*!
 * \brief Function executed on Radio Tx Timeout event
 */
void OnTxTimeout(void);

/*!
 * \brief Function executed on Radio Rx Timeout event
 */
void OnRxTimeout(void);

/*!
 * \brief Function executed on Radio Rx Error event
 */
void OnRxError(void);

/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Exported functions --------------------------------------------------------*/
int32_t TST_TxTone(uint32_t freq, int32_t power)
{
  /* USER CODE BEGIN TST_TxTone_1 */

  /* USER CODE END TST_TxTone_1 */
  if ((TestState & TX_TEST_TONE) != TX_TEST_TONE)
  {
    TestState |= TX_TEST_TONE;

    PRINTF("Tx Continuous Test\r\n");

    Radio.SetTxContinuousWave(freq, power, CONTINUOUS_TIMEOUT);

    return 0;
  }
  else
  {
    return -1;
  }
  /* USER CODE BEGIN TST_TxTone_2 */

  /* USER CODE END TST_TxTone_2 */
}

int32_t TST_RxRssi(void)
{
  /* USER CODE BEGIN TST_RxRssi_1 */

  /* USER CODE END TST_RxRssi_1 */
  uint32_t timeout = 0;
  int16_t rssiVal = 0;
  RxConfigGeneric_t RxConfig;
  /* Test with LNA */
  /* check that test is not already started*/
  if ((TestState & RX_TEST_RSSI) != RX_TEST_RSSI)
  {
    TestState |= RX_TEST_RSSI;

    PRINTF("Rx FSK Test\r\n");

    Radio.SetChannel(testParam.freq);

    /* RX Continuous */
    uint8_t syncword[] = { 0xC1, 0x94, 0xC1, 0x00, 0x00, 0x00, 0x00, 0x00 };
    RxConfig.fsk.ModulationShaping = (RADIO_FSK_ModShapings_t)((testParam.BTproduct == 0) ? 0 : testParam.BTproduct + 8);
    RxConfig.fsk.Bandwidth = testParam.bandwidth;
    RxConfig.fsk.BitRate = testParam.loraSf_datarate; /*BitRate*/
    RxConfig.fsk.PreambleLen = 3;   /*in Byte*/
    RxConfig.fsk.SyncWordLength = 3; /*in Byte*/
    RxConfig.fsk.SyncWord = syncword; /*SyncWord Buffer*/
    RxConfig.fsk.whiteSeed = 0x01FF ; /*WhiteningSeed*/
    RxConfig.fsk.LengthMode = RADIO_FSK_PACKET_VARIABLE_LENGTH; /* If the header is explicit, it will be transmitted in the GFSK packet. If the header is implicit, it will not be transmitted*/
    RxConfig.fsk.CrcLength = RADIO_FSK_CRC_2_BYTES_CCIT;       /* Size of the CRC block in the GFSK packet*/
    RxConfig.fsk.CrcPolynomial = 0x1021;
    RxConfig.fsk.Whitening = RADIO_FSK_DC_FREEWHITENING;
    Radio.RadioSetRxGenericConfig(GENERIC_FSK, &RxConfig, RX_CONTINUOUS_ON, 0);

    timeout = 0xFFFFFF; /* continuous Rx */
    if (testParam.lna == 0)
    {
      Radio.Rx(timeout);
    }
    else
    {
      Radio.RxBoosted(timeout);
    }

    HAL_Delay(Radio.GetWakeupTime());   /* Wait for 50ms */

    rssiVal = Radio.Rssi(MODEM_FSK);
    PRINTF(">>> RSSI Value= %d dBm\r\n", rssiVal);

    Radio.Sleep();
    TestState &= ~RX_TEST_RSSI;
    return 0;
  }
  else
  {
    return -1;
  }
  /* USER CODE BEGIN TST_RxRssi_2 */

  /* USER CODE END TST_RxRssi_2 */
}

int32_t  TST_set_config(testParameter_t *Param)
{
  /* USER CODE BEGIN TST_set_config_1 */

  /* USER CODE END TST_set_config_1 */
  UTIL_MEM_cpy_8(&testParam, Param, sizeof(testParameter_t));

  return 0;
  /* USER CODE BEGIN TST_set_config_2 */

  /* USER CODE END TST_set_config_2 */
}

int32_t TST_get_config(testParameter_t *Param)
{
  /* USER CODE BEGIN TST_get_config_1 */

  /* USER CODE END TST_get_config_1 */
  UTIL_MEM_cpy_8(Param, &testParam, sizeof(testParameter_t));
  return 0;
  /* USER CODE BEGIN TST_get_config_2 */

  /* USER CODE END TST_get_config_2 */
}

int32_t TST_stop(void)
{
  /* USER CODE BEGIN TST_stop_1 */

  /* USER CODE END TST_stop_1 */
  TestState = 0;

  /* Set the radio in Sleep*/
  Radio.Sleep();

  return 0;
  /* USER CODE BEGIN TST_stop_2 */

  /* USER CODE END TST_stop_2 */
}



static void txtt_emit(void)
{
    RadioState_t rs = Radio.GetStatus();

    if (!TestRF_Active) {
        Radio.Sleep( );
        return;
    }

    if (TestRF_tm == TM_LBT) {
        if( RadioIsChannelFreeLBT( TestRF_TxFrequency, 0, TestRF_LbtRssiFreeTh, TestRF_LbtCarrierSenseTime ) == true ) {
            //PRINTF("Transmitting after %d ms ...\r\n\n", lbt_send_delay);
            HAL_Delay(TestRF_LbtSendDelay);
            if (rs != RF_TX_RUNNING) {
                PRINTF("Begin transmitting ...\r\n");
                Radio.Send((uint8_t*)TestRF_RxData, TestRF_TxSize);
                PRINTF("End transmitting ...\r\n");
            }
            else {
                PRINTF("On transmitting ...\r\n");
            }
        }
        else {
            while (!TestRF_TxTimerPeriod) {
                PRINTF("[LBT] Carrier busy freq(%d)\r\n", TestRF_TxFrequency);

                if( RadioIsChannelFreeLBT(TestRF_TxFrequency, 0, TestRF_LbtRssiFreeTh, TestRF_LbtCarrierSenseTime ) == true ) {
                    //PRINTF("[LBT] Free channel(%d) found\r\n", TxFrequency);
                    if (rs != RF_TX_RUNNING) {
                        PRINTF("Begin transmitting ...\r\n");
                        Radio.Send((uint8_t*)TestRF_RxData, TestRF_TxSize);
                        PRINTF("End transmitting ...\r\n");
                    }
                    else {
                        PRINTF("On transmitting ...\r\n");
                    }
                    break;
                }
                else {
                    PRINTF("[LBT] Carrier busy freq(%d)\r\n", TestRF_TxFrequency);
                }
            }
        }
    }
}



static int rxtx_state = 0;
static void rxtxtt_emit(void)
{
    int nTimer = 500;
    static int nRxWaitCnt = 10;
    if(rxtx_state == 0) {
        
        PRINTF("rxtxtt_emit start \r\n");
        
        RadioTxDone_flag = 0;
        RadioRxDone_flag = 0;
        RadioTxTimeout_flag = 0;
        RadioRxTimeout_flag = 0;
        RadioError_flag = 0;
        
        Radio.SetChannel(testParam.freq);

        PRINTF("SetTxConfig\r\n");
        Radio.SetTxConfig( MODEM_LORA, 14, 0, 0/*0:125, 1:250, 2:500 kHz*/,
                            12, LORA_CODINGRATE, LORA_PREAMBLE_LENGTH, LORA_FIX_LENGTH_PAYLOAD_OFF,
                            false, 0, 0, LORA_IQ_INVERSION_OFF, 5000 );

        /* Send payload once*/
        PRINTF("Radio.Send\r\n");
        Radio.Send(payload, testParam.payloadLen);

        nTimer = 5000;
        rxtx_state = 1;
    }
    else if(rxtx_state == 1) {
        
        if (RadioTxDone_flag == 1) {
            PRINTF("RadioTxDone_flag - OnTxDone\r\n");
            Radio.Sleep();
            rxtx_state = 2;
        }
        else if (RadioTxTimeout_flag == 1) {
            PRINTF("RadioTxTimeout_flag - OnTxTimeout\r\n");
            Radio.Sleep();
            rxtx_state = 2;
        }
        else if (RadioError_flag == 1) {
            PRINTF("RadioError_flag- OnRxError\r\n");
            Radio.Sleep();
            rxtx_state = 2;
        }
        else {
            PRINTF("RadioTxDone_flag - waiting\r\n");
        }
        
    }
    else if(rxtx_state == 2) {
        RadioTxDone_flag = 0;
        RadioRxDone_flag = 0;
        RadioTxTimeout_flag = 0;
        RadioRxTimeout_flag = 0;
        RadioError_flag = 0;
    
        PRINTF("RX-SetChannel\r\n");
        Radio.SetChannel(testParam.freq);

        
        PRINTF("SetRxConfig\r\n");
        Radio.SetRxConfig( MODEM_LORA, 0, 12,
                        LORA_CODINGRATE, 0, LORA_PREAMBLE_LENGTH,
                        LORA_SYMBOL_TIMEOUT, LORA_FIX_LENGTH_PAYLOAD_OFF,
                        0, true, 0, 0, LORA_IQ_INVERSION_OFF, true );
                
        PRINTF("SetMaxPayloadLength\r\n");
        Radio.SetMaxPayloadLength( MODEM_LORA, 250);
        
        PRINTF("Radio.Rx\r\n");
        Radio.Rx( 0 );
        nTimer = 5000;

        nRxWaitCnt = 10;
        rxtx_state = 3;
    }
    
    else if(rxtx_state == 3) {
        
        if (RadioRxDone_flag == 1) {
            PRINTF("RadioRxDone_flag-OnRxDone\r\n");
            Radio.Sleep();
            rxtx_state = 0;
        }
        else if (RadioRxTimeout_flag == 1) {
            PRINTF("RadioRxDone_flag-OnRxTimeout\r\n");
            Radio.Sleep();
            rxtx_state = 0;
        }
        else if (RadioError_flag == 1) {
            PRINTF("RadioRxDone_flag-OnRxError\r\n");
            Radio.Sleep();
            rxtx_state = 0;
        }
        else {
            
            if(nRxWaitCnt-- < 0)  {
                PRINTF("RadioRxDone_flag - end\r\n");
                Radio.Sleep();
                rxtx_state = 0;
            }
            else {
                PRINTF("RadioRxDone_flag - waiting\r\n");
            }
        }
    }

    PRINTF("Next Timer %d\r\n", nTimer);
    TimerSetValue( &TestRF_RxTxTimer, nTimer );
    TimerStart( &TestRF_RxTxTimer );


}


static void OnTxTimerExpired(void *context)
{
    if (!TestRF_Active) {
        Radio.Sleep( );
        return;
    }

    TimerStart( &TestRF_TxTimer );
    txtt_emit();
}



static void OnRxTxTimerExpired(void *context)
{
    rxtxtt_emit();
}





bool TST_TX_Start_LBT(uint8_t TxID, uint32_t freq, int8_t power, uint8_t sf, uint8_t bandwidth, uint8_t data_size, uint32_t period)
{
    uint32_t timeout = 10000;

    TestRF_RxData[0] = TxID;
    TestRF_TxFrequency = freq;
    TestRF_TxSize = data_size;
    TestRF_TxTimerPeriod = period;


    switch (TestRF_Region) {
        case LORAMAC_REGION_AS923:
        break;

        case LORAMAC_REGION_KR920:
            TestRF_tm = TM_LBT;
            TestRF_LbtRssiFreeTh = KR920_RSSI_FREE_TH;
            TestRF_LbtCarrierSenseTime = KR920_CARRIER_SENSE_TIME;
            TestRF_LbtSendDelay = 0;
            break;

        case LORAMAC_REGION_EU868:
            break;

        case LORAMAC_REGION_US915:
            break;

        default:
            TestRF_tm = TM_NORMAL;
            break;
    }

    TestRF_Active = true;

    if (TestRF_TxSize > MAX_RECEIVED_DATA){
        TestRF_TxSize = MAX_RECEIVED_DATA -1;
    }

    /* Radio initialization */
    RadioEvents.TxDone = OnTxDone;
    RadioEvents.RxDone = NULL;
    RadioEvents.TxTimeout = OnTxTimeout;
    RadioEvents.RxTimeout = NULL;
    RadioEvents.RxError = OnRxError;
    Radio.Init(&RadioEvents);
    Radio.SetChannel(TestRF_TxFrequency);

    Radio.SetTxConfig( MODEM_LORA, power, 0, bandwidth/*0:125, 1:250, 2:500 kHz*/,
                        sf, LORA_CODINGRATE, LORA_PREAMBLE_LENGTH, LORA_FIX_LENGTH_PAYLOAD_OFF,
                        false, 0, 0, LORA_IQ_INVERSION_OFF, timeout );


    // Setup maximum payload lenght of the radio driver
    Radio.SetMaxPayloadLength( MODEM_LORA, TestRF_TxSize );

    // Get the time-on-air of the next tx frame
    TestRF_TxTimeOnAir = Radio.TimeOnAir( MODEM_LORA, bandwidth, sf,1,8, false,TestRF_TxSize,false );

    PRINTF("time-on-air %u ms\r\n", TestRF_TxTimeOnAir);


    if (TestRF_TxTimerPeriod < TestRF_TxTimeOnAir) {
        TestRF_TxTimerPeriod = 0;
    }

    TimerInit( &TestRF_TxTimer, OnTxTimerExpired );

    if (TestRF_TxTimerPeriod > 0){
        TimerSetValue( &TestRF_TxTimer, TestRF_TxTimerPeriod );
        TimerStart( &TestRF_TxTimer );
        PRINTF("TX Timer starting \r\n");
    }

    txtt_emit();

    return true;
}




bool TST_RX_Start_LBT(uint32_t freq, uint8_t sf, uint32_t bandwidth)
{
       
    TestRF_Active = true;

    RadioEvents.TxDone = NULL;
    RadioEvents.RxDone = OnRxDone;
    RadioEvents.TxTimeout = NULL;
    RadioEvents.RxTimeout = NULL;
    RadioEvents.RxError = OnRxError;
    Radio.Init( &RadioEvents );
    Radio.SetChannel( freq );

    if (sf == 50) {
        Radio.SetRxConfig( MODEM_FSK, FSK_BANDWIDTH, FSK_DATARATE,
                            0, FSK_AFC_BANDWIDTH, FSK_PREAMBLE_LENGTH,
                            0, FSK_FIX_LENGTH_PAYLOAD_ON, 0, true,
                            0, 0,false, true );
    }
    else {

        Radio.SetRxConfig( MODEM_LORA, bandwidth, sf,
                            LORA_CODINGRATE, 0, LORA_PREAMBLE_LENGTH,
                            LORA_SYMBOL_TIMEOUT, LORA_FIX_LENGTH_PAYLOAD_OFF,
                            0, true, 0, 0, LORA_IQ_INVERSION_OFF, true );
        
    }

    Radio.SetMaxPayloadLength( MODEM_LORA, 250);
    Radio.Rx( 0 ); // Continuous Rx

    return true;
}



int32_t TST_TX_Start(int32_t nb_packet)
{
  /* USER CODE BEGIN TST_TX_Start_1 */

  /* USER CODE END TST_TX_Start_1 */
  int32_t i;
  TxConfigGeneric_t TxConfig;

  if ((TestState & TX_TEST_LORA) != TX_TEST_LORA)
  {
    TestState |= TX_TEST_LORA;

    PRINTF("Tx LoRa Test\r\n");

    /* Radio initialization */
    RadioEvents.TxDone = OnTxDone;
    RadioEvents.RxDone = OnRxDone;
    RadioEvents.TxTimeout = OnTxTimeout;
    RadioEvents.RxTimeout = OnRxTimeout;
    RadioEvents.RxError = OnRxError;
    Radio.Init(&RadioEvents);
    /*Fill payload with PRBS9 data*/
    Prbs9_generator(payload, testParam.payloadLen);

    /* Launch several times payload: nb times given by user */
    for (i = 1; i <= nb_packet; i++)
    {
      PRINTF("Tx %d of %d\r\n", i, nb_packet);
      Radio.SetChannel(testParam.freq);

      if (testParam.modulation == TEST_FSK)
      {
        /*fsk modulation*/
        uint8_t syncword[] = { 0xC1, 0x94, 0xC1, 0x00, 0x00, 0x00, 0x00, 0x00 };
        TxConfig.fsk.ModulationShaping = (RADIO_FSK_ModShapings_t)((testParam.BTproduct == 0) ? 0 : testParam.BTproduct + 7);
        TxConfig.fsk.Bandwidth = testParam.bandwidth;
        TxConfig.fsk.FrequencyDeviation = testParam.fskDev;
        TxConfig.fsk.BitRate = testParam.loraSf_datarate; /*BitRate*/
        TxConfig.fsk.PreambleLen = 3;   /*in Byte        */
        TxConfig.fsk.SyncWordLength = 3; /*in Byte        */
        TxConfig.fsk.SyncWord = syncword; /*SyncWord Buffer*/
        TxConfig.fsk.whiteSeed = 0x01FF ; /*WhiteningSeed  */
        TxConfig.fsk.HeaderType = RADIO_FSK_PACKET_VARIABLE_LENGTH; /* If the header is explicit, it will be transmitted in the GFSK packet. If the header is implicit, it will not be transmitted*/
        TxConfig.fsk.CrcLength = RADIO_FSK_CRC_2_BYTES_CCIT;       /* Size of the CRC block in the GFSK packet*/
        TxConfig.fsk.CrcPolynomial = 0x1021;
        TxConfig.fsk.Whitening = RADIO_FSK_DC_FREE_OFF;
        Radio.RadioSetTxGenericConfig(GENERIC_FSK, &TxConfig, testParam.power, TX_TIMEOUT_VALUE);
      }
      else if (testParam.modulation == TEST_LORA)
      {
        /*lora modulation*/
        TxConfig.lora.Bandwidth = (RADIO_LoRaBandwidths_t) testParam.bandwidth;
        TxConfig.lora.SpreadingFactor = (RADIO_LoRaSpreadingFactors_t) testParam.loraSf_datarate; /*BitRate*/
        TxConfig.lora.Coderate = (RADIO_LoRaCodingRates_t)testParam.codingRate;
        TxConfig.lora.LowDatarateOptimize = (RADIO_Ld_Opt_t)testParam.lowDrOpt; /*0 inactive, 1 active, 2: auto*/
        TxConfig.lora.PreambleLen = LORA_PREAMBLE_LENGTH;
        TxConfig.lora.LengthMode = RADIO_LORA_PACKET_VARIABLE_LENGTH;
        TxConfig.lora.CrcMode = RADIO_LORA_CRC_ON;
        TxConfig.lora.IqInverted = RADIO_LORA_IQ_NORMAL;
        Radio.RadioSetTxGenericConfig(GENERIC_LORA, &TxConfig, testParam.power, TX_TIMEOUT_VALUE);
      }
      else if (testParam.modulation == TEST_BPSK)
      {
        TxConfig.bpsk.BitRate = testParam.loraSf_datarate; /*BitRate*/
        Radio.RadioSetTxGenericConfig(GENERIC_BPSK, &TxConfig, testParam.power, TX_TIMEOUT_VALUE);
      }
      else
      {
        return -1; /*error*/
      }
      /* Send payload once*/
      Radio.Send(payload, testParam.payloadLen);
      /* Wait Tx done/timeout */
      UTIL_SEQ_WaitEvt(1 << CFG_SEQ_Evt_RadioOnTstRF);
      Radio.Sleep();

      if (RadioTxDone_flag == 1)
      {
        PRINTF("OnTxDone\r\n");
      }

      if (RadioTxTimeout_flag == 1)
      {
        PRINTF("OnTxTimeout\r\n");
      }

      if (RadioError_flag == 1)
      {
        PRINTF("OnRxError\r\n");
      }

      /*Delay between 2 consecutive Tx*/
      HAL_Delay(500);
      /* Reset TX Done or timeout flags */
      RadioTxDone_flag = 0;
      RadioTxTimeout_flag = 0;
      RadioError_flag = 0;
    }
    TestState &= ~TX_TEST_LORA;
    return 0;
  }
  else
  {
    return -1;
  }
  /* USER CODE BEGIN TST_TX_Start_2 */

  /* USER CODE END TST_TX_Start_2 */
}

int32_t TST_RX_Start(int32_t nb_packet)
{
  /* USER CODE BEGIN TST_RX_Start_1 */

  /* USER CODE END TST_RX_Start_1 */
  int32_t i;
  /* init of PER counter */
  uint32_t count_RxOk = 0;
  uint32_t count_RxKo = 0;
  uint32_t PER = 0;
  RxConfigGeneric_t RxConfig = {0};

  if (((TestState & RX_TEST_LORA) != RX_TEST_LORA) && (nb_packet > 0))
  {
    TestState |= RX_TEST_LORA;

    /* Radio initialization */
    RadioEvents.TxDone = OnTxDone;
    RadioEvents.RxDone = OnRxDone;
    RadioEvents.TxTimeout = OnTxTimeout;
    RadioEvents.RxTimeout = OnRxTimeout;
    RadioEvents.RxError = OnRxError;
    Radio.Init(&RadioEvents);

    for (i = 1; i <= nb_packet; i++)
    {
      /* Rx config */
      Radio.SetChannel(testParam.freq);

      if (testParam.modulation == TEST_FSK)
      {
        /*fsk modulation*/
        uint8_t syncword[] = { 0xC1, 0x94, 0xC1, 0x00, 0x00, 0x00, 0x00, 0x00 };
        RxConfig.fsk.ModulationShaping = (RADIO_FSK_ModShapings_t)((testParam.BTproduct == 0) ? 0 : testParam.BTproduct + 8);
        RxConfig.fsk.Bandwidth = testParam.bandwidth;
        RxConfig.fsk.BitRate = testParam.loraSf_datarate; /*BitRate*/
        RxConfig.fsk.PreambleLen = 3; /*in Byte*/
        RxConfig.fsk.SyncWordLength = 3; /*in Byte*/
        RxConfig.fsk.SyncWord = syncword; /*SyncWord Buffer*/
        RxConfig.fsk.PreambleMinDetect = RADIO_FSK_PREAMBLE_DETECTOR_08_BITS;
        RxConfig.fsk.whiteSeed = 0x01FF ; /*WhiteningSeed*/
        RxConfig.fsk.LengthMode = RADIO_FSK_PACKET_VARIABLE_LENGTH; /* If the header is explicit, it will be transmitted in the GFSK packet. If the header is implicit, it will not be transmitted*/
        RxConfig.fsk.CrcLength = RADIO_FSK_CRC_2_BYTES_CCIT;       /* Size of the CRC block in the GFSK packet*/
        RxConfig.fsk.CrcPolynomial = 0x1021;
        RxConfig.fsk.Whitening = RADIO_FSK_DC_FREE_OFF;
        RxConfig.fsk.MaxPayloadLength = 255;
        RxConfig.fsk.AddrComp = RADIO_FSK_ADDRESSCOMP_FILT_OFF;
        Radio.RadioSetRxGenericConfig(GENERIC_FSK, &RxConfig, RX_CONTINUOUS_ON, 0);
      }
      else if (testParam.modulation == TEST_LORA)
      {
        /*Lora*/
        RxConfig.lora.Bandwidth = (RADIO_LoRaBandwidths_t) testParam.bandwidth;
        RxConfig.lora.SpreadingFactor = (RADIO_LoRaSpreadingFactors_t) testParam.loraSf_datarate; /*BitRate*/
        RxConfig.lora.Coderate = (RADIO_LoRaCodingRates_t)testParam.codingRate;
        RxConfig.lora.LowDatarateOptimize = (RADIO_Ld_Opt_t)testParam.lowDrOpt; /*0 inactive, 1 active, 2: auto*/
        RxConfig.lora.PreambleLen = LORA_PREAMBLE_LENGTH;
        RxConfig.lora.LengthMode = RADIO_LORA_PACKET_VARIABLE_LENGTH;
        RxConfig.lora.CrcMode = RADIO_LORA_CRC_ON;
        RxConfig.lora.IqInverted = RADIO_LORA_IQ_NORMAL;
        Radio.RadioSetRxGenericConfig(GENERIC_LORA, &RxConfig, RX_CONTINUOUS_ON, LORA_SYMBOL_TIMEOUT);
      }
      else
      {
        return -1; /*error*/
      }

      Radio.Rx(RX_TIMEOUT_VALUE);

      /* Wait Rx done/timeout */
      UTIL_SEQ_WaitEvt(1 << CFG_SEQ_Evt_RadioOnTstRF);
      Radio.Sleep();

      if (RadioRxDone_flag == 1)
      {
        int16_t rssi = last_rx_rssi;
        int8_t LoraSnr_FskCfo = last_rx_LoraSnr_FskCfo;
        PRINTF("OnRxDone\r\n");
        if (testParam.modulation == TEST_FSK)
        {
          PRINTF("RssiValue=%d dBm, cfo=%dkHz\r\n", rssi, LoraSnr_FskCfo);
        }
        else
        {
          PRINTF("RssiValue=%d dBm, SnrValue=%ddB\r\n", rssi, LoraSnr_FskCfo);
        }
      }

      if (RadioRxTimeout_flag == 1)
      {
        PRINTF("OnRxTimeout\r\n");
      }

      if (RadioError_flag == 1)
      {
        PRINTF("OnRxError\r\n");
      }

      /*check flag*/
      if ((RadioRxTimeout_flag == 1) || (RadioError_flag == 1))
      {
        count_RxKo++;
      }
      if (RadioRxDone_flag == 1)
      {
        count_RxOk++;
      }
      /* Reset timeout flag */
      RadioRxDone_flag = 0;
      RadioRxTimeout_flag = 0;
      RadioError_flag = 0;

      /* Compute PER */
      PER = (100 * (count_RxKo)) / (count_RxKo + count_RxOk);
      PRINTF("Rx %d of %d  >>> PER= %d %%\r\n", i, nb_packet, PER);
    }
    TestState &= ~RX_TEST_LORA;
    return 0;
  }
  else
  {
    return -1;
  }
  /* USER CODE BEGIN TST_RX_Start_2 */

  /* USER CODE END TST_RX_Start_2 */
}




int32_t TST_RXTX_Start(void)
{
    TestState = TX_TEST_LORA;
    PRINTF("Rx-Tx Stress LoRa Test\r\n");
    

    /* Radio initialization */
    RadioEvents.TxDone = OnTxDone;
    RadioEvents.RxDone = OnRxDone;
    RadioEvents.TxTimeout = OnTxTimeout;
    RadioEvents.RxTimeout = OnRxTimeout;
    RadioEvents.RxError = OnRxError;
    
    Radio.Init(&RadioEvents);


    testParam.payloadLen = 50;
    testParam.modulation = TEST_LORA;
    
    Prbs9_generator(payload, testParam.payloadLen);

    TimerInit( &TestRF_RxTxTimer, OnRxTxTimerExpired );
    TimerSetValue( &TestRF_RxTxTimer, 100 );
    
    TimerStart( &TestRF_RxTxTimer );
    return 0;
    
}




/* Private Functions Definition -----------------------------------------------*/

void OnTxDone(void)
{
    PRINTFD("OnTxDone \r\n");
    RadioTxDone_flag = 1;
    //UTIL_SEQ_SetEvt(1 << CFG_SEQ_Evt_RadioOnTstRF);

}

void OnRxDone(uint8_t *payload, uint16_t size, int16_t rssi, int8_t LoraSnr_FskCfo)
{
  last_rx_rssi = rssi;
  last_rx_LoraSnr_FskCfo = LoraSnr_FskCfo;

  /* Set Rxdone flag */
  RadioRxDone_flag = 1;
  PRINTF("[EVENT] RxDone size %d rssi %d snr %d\r\n", size, rssi, LoraSnr_FskCfo);
  //UTIL_SEQ_SetEvt(1 << CFG_SEQ_Evt_RadioOnTstRF);

}

void OnTxTimeout(void)
{
  RadioTxTimeout_flag = 1;
  PRINTFD("OnTxTimeout \r\n");
  //UTIL_SEQ_SetEvt(1 << CFG_SEQ_Evt_RadioOnTstRF);
  
  
}

void OnRxTimeout(void)
{
  PRINTF("OnRxTimeout\r\n");
  RadioRxTimeout_flag = 1;
  //UTIL_SEQ_SetEvt(1 << CFG_SEQ_Evt_RadioOnTstRF);
  
  
}

void OnRxError(void)
{
  PRINTF("OnRxError\r\n");
  RadioError_flag = 1;
  //UTIL_SEQ_SetEvt(1 << CFG_SEQ_Evt_RadioOnTstRF);

}

static int32_t Prbs9_generator(uint8_t *payload, uint8_t len)
{
  /* USER CODE BEGIN Prbs9_generator_1 */

  /* USER CODE END Prbs9_generator_1 */
  uint16_t prbs9_val = PRBS9_INIT;
  /*init payload to 0*/
  UTIL_MEM_set_8(payload, 0, len);

  for (int32_t i = 0; i < len * 8; i++)
  {
    /*fill buffer with prbs9 sequence*/
    int32_t newbit = (((prbs9_val >> 8) ^ (prbs9_val >> 4)) & 1);
    prbs9_val = ((prbs9_val << 1) | newbit) & 0x01ff;
    payload[i / 8] |= ((prbs9_val & 0x1) << (i % 8));
  }
  return 0;
  /* USER CODE BEGIN Prbs9_generator_2 */

  /* USER CODE END Prbs9_generator_2 */
}

/* USER CODE BEGIN PrFD */

/* USER CODE END PrFD */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
