/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file    lora_at.h
  * @author  MCD Application Team
  * @brief   Header for driver at.c module
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under Ultimate Liberty license
  * SLA0044, the "License"; You may not use this file except in compliance with
  * the License. You may obtain a copy of the License at:
  *                             www.st.com/SLA0044
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __LORA_AT_H__
#define __LORA_AT_H__



/* Includes ------------------------------------------------------------------*/
#include "LmHandler.h"
#include "stm32_adv_trace.h"
#include "mw_log_conf.h"



#define LORA_APP_VERSION            1



/* Exported types ------------------------------------------------------------*/
/*
 * AT Command Id errors. Note that they are in sync with ATError_description static array
 * in command.c
 */
typedef enum eATEerror
{
  AT_OK = 0,
  AT_ERROR,
  AT_PARAM_ERROR,
  AT_BUSY_ERROR,
  AT_TEST_PARAM_OVERFLOW,
  AT_NO_NET_JOINED,
  AT_RX_ERROR,
  AT_NO_CLASS_B_ENABLE,
  AT_DUTYCYCLE_RESTRICTED,
  AT_CRYPTO_ERROR,
  AT_MAX,
} ATError_t;

/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* External variables --------------------------------------------------------*/
/* USER CODE BEGIN EV */

/* USER CODE END EV */

/* Exported macro ------------------------------------------------------------*/
/* AT printf */
#define AT_PRINTF           PRINTF
#define AT_PRINTFD          PRINTFD


/* AT Command strings. Commands start with AT */
/* General commands */
#define AT_VER              "+VER"
#define AT_RESET            "+RESET"
#define AT_FSET             "+FSET"
#define AT_TEST             "+TEST"
#define AT_DBGL             "+DBGL"
#define AT_CHANNEL          "+CHANNEL"
#define AT_RATESET          "+RATESET"
#define AT_NODEID           "+NODEID"
#define AT_RFPOWER          "+RFPOWER"
#define AT_SEND             "+SEND"
#define AT_BDLOAD           "+BDLOAD"
#define AT_DEVEUI           "+DEVEUI"










void    SetATCommandInfo(void *pInfo);
void    print_8_02x(uint8_t *pt);


ATError_t at_return_ok(const char *param);
ATError_t at_return_error(const char *param);
ATError_t at_version(const char *param);
ATError_t at_reset(const char *param);
ATError_t at_fset(const char *param);
ATError_t at_test(const char *param);
ATError_t at_verbose_get(const char *param);
ATError_t at_verbose_set(const char *param);
ATError_t at_get_p2p_channel(const char *param);
ATError_t at_set_p2p_channel(const char *param);
ATError_t at_get_p2p_rateset(const char *param);
ATError_t at_set_p2p_rateset(const char *param);
ATError_t at_get_p2p_node_id(const char *param);
ATError_t at_set_p2p_node_id(const char *param);
ATError_t at_set_p2p_reset(const char *param);
ATError_t at_set_p2p_fset(const char *param);
ATError_t at_get_p2p_rfpower(const char *param);
ATError_t at_set_p2p_rfpower(const char *param);
ATError_t at_set_p2p_send(const char *param);
ATError_t at_set_p2p_bdload(const char *param);
ATError_t at_set_p2p_deveui(const char *param);
ATError_t at_get_p2p_deveui(const char *param);







#endif /* __LORA_AT_H__ */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
