/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file    lora_command.c
  * @author  MCD Application Team
  * @brief   Main command driver dedicated to command AT
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under Ultimate Liberty license
  * SLA0044, the "License"; You may not use this file except in compliance with
  * the License. You may obtain a copy of the License at:
  *                             www.st.com/SLA0044
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include <string.h>
#include "platform.h"
#include "lora_at.h"
#include "lora_command.h"

/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* External variables ---------------------------------------------------------*/
/* USER CODE BEGIN EV */

/* USER CODE END EV */

/* comment the following to have help message */
/* #define NO_HELP */
/* #define AT_RADIO_ACCESS */

/* Private typedef -----------------------------------------------------------*/
/**
  * @brief  Structure defining an AT Command
  */
struct ATCommand_s
{
  const char *string;                       /*< command string, after the "AT" */
  const int32_t size_string;                /*< size of the command string, not including the final \0 */
  ATError_t (*get)(const char *param);     /*< =? after the string to get the current value*/
  ATError_t (*set)(const char *param);     /*< = (but not =?\0) after the string to set a value */
  ATError_t (*run)(const char *param);     /*< \0 after the string - run the command */
};

/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
#define CMD_SIZE                        540
#define CIRC_BUFF_SIZE                  8

/* USER CODE BEGIN PD */

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/

/**
  * @brief  Array corresponding to the description of each possible AT Error
  */
static const char *const ATError_description[] =
{
  "\r\nOK\r\n",                     /* AT_OK */
  "\r\nAT_ERROR\r\n",               /* AT_ERROR */
  "\r\nAT_PARAM_ERROR\r\n",         /* AT_PARAM_ERROR */
  "\r\nAT_BUSY_ERROR\r\n",          /* AT_BUSY_ERROR */
  "\r\nAT_TEST_PARAM_OVERFLOW\r\n", /* AT_TEST_PARAM_OVERFLOW */
  "\r\nAT_NO_NETWORK_JOINED\r\n",   /* AT_NO_NET_JOINED */
  "\r\nAT_RX_ERROR\r\n",            /* AT_RX_ERROR */
  "\r\nAT_NO_CLASS_B_ENABLE\r\n",   /* AT_NO_CLASS_B_ENABLE */
  "\r\nAT_DUTYCYCLE_RESTRICTED\r\n", /* AT_DUTYCYCLE_RESTRICTED */
  "\r\nAT_CRYPTO_ERROR\r\n",        /* AT_CRYPTO_ERROR */
  "\r\nerror unknown\r\n",          /* AT_MAX */
};

/**
  * @brief  Array of all supported AT Commands
  */
static const struct ATCommand_s ATCommand[] =
{
    {
        .string = AT_VER,
        .size_string = sizeof(AT_VER) - 1,
        .get = at_version,
        .set = at_version,
        .run = at_version,
    },
    {
        .string = AT_RESET,
        .size_string = sizeof(AT_RESET) - 1,
        .get = at_reset,
        .set = at_reset,
        .run = at_reset,
    },
    {
        .string = AT_FSET,
        .size_string = sizeof(AT_FSET) - 1,
        .get = at_fset,
        .set = at_fset,
        .run = at_fset,
    },
    {
        .string = AT_TEST,
        .size_string = sizeof(AT_TEST) - 1,
        .get = at_test,
        .set = at_test,
        .run = at_test,
    },
    {
        .string = AT_DBGL,
        .size_string = sizeof(AT_DBGL) - 1,
        .get = at_verbose_get,
        .set = at_verbose_set,
        .run = at_verbose_get,
    },
    {
        .string = AT_CHANNEL,
        .size_string = sizeof(AT_CHANNEL) - 1,
        .get = at_get_p2p_channel,
        .set = at_set_p2p_channel,
        .run = at_get_p2p_channel,
    },
    {
        .string = AT_RATESET,
        .size_string = sizeof(AT_RATESET) - 1,
        .get = at_get_p2p_rateset,
        .set = at_set_p2p_rateset,
        .run = at_get_p2p_rateset,
    },
    {
        .string = AT_NODEID,
        .size_string = sizeof(AT_NODEID) - 1,
        .get = at_get_p2p_node_id,
        .set = at_set_p2p_node_id,
        .run = at_get_p2p_node_id,
    },
    {
        .string = AT_RFPOWER,
        .size_string = sizeof(AT_RFPOWER) - 1,
        .get = at_get_p2p_rfpower,
        .set = at_set_p2p_rfpower,
        .run = at_get_p2p_rfpower,
    },
    {
        .string = AT_SEND,
        .size_string = sizeof(AT_SEND) - 1,
        .get = at_set_p2p_send,
        .set = at_set_p2p_send,
        .run = at_set_p2p_send,
    },
    {
        .string = AT_BDLOAD,
        .size_string = sizeof(AT_BDLOAD) - 1,
        .get = at_set_p2p_bdload,
        .set = at_set_p2p_bdload,
        .run = at_set_p2p_bdload,
    },
    {
        .string = AT_DEVEUI,
        .size_string = sizeof(AT_DEVEUI) - 1,
        .get = at_get_p2p_deveui,
        .set = at_set_p2p_deveui,
        .run = at_get_p2p_deveui,
    },
};





static char circBuffer[CIRC_BUFF_SIZE+3];
static char command[CMD_SIZE+3];
static unsigned cmd_index = 0;
static uint32_t widx = 0;
static uint32_t ridx = 0;


/* USER CODE BEGIN PV */

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/

/**
  * @brief  Parse a command and process it
  * @param  cmd The command
  */
static void parse_at_cmd(const char *cmd);

/**
  * @brief  Print a string corresponding to an ATError_t
  * @param  error_type The AT error code
  */
static void com_error(ATError_t error_type);

/**
  * @brief  CMD_GetChar callback from ADV_TRACE
  * @param  rxChar th char received
  * @param  size
  * @param  error
  */
static void CMD_GetChar(uint8_t *rxChar, uint16_t size, uint8_t error);

/**
  * @brief  CNotifies the upper layer that a character has been received
  */
static void (*NotifyCb)(void);

/**
  * @brief  Remove backspace and its preceding character in the Command string
  * @param  cmd string to process
  * @retval 0 when OK, otherwise error
  */


/* Exported functions --------------------------------------------------------*/
void CMD_Init(void (*CmdProcessNotify)(void))
{
    UTIL_ADV_TRACE_StartRxProcess(CMD_GetChar);
    /* register call back*/
    if (CmdProcessNotify != NULL) {
        NotifyCb = CmdProcessNotify;
    }
    widx = 0;
    ridx = 0;
}





void CMD_AT_Process(void)
{
    bool bProcess = true;

    while (bProcess) {
        
        if(ridx == widx) {
            break;
        }

        if(cmd_index>=9 && (memcmp(command, "AT+SENDH=", 9) == 0) ) {  // AT+SENDB=PORT,TYPE,LENG,DATA
            uint8_t MsgLength = 15;
            if( cmd_index >= MsgLength ) {
                MsgLength = 9 + 3 + command[11];
                
                if(cmd_index >= MsgLength) {
                    // com_error(AT_SendH((const char *)&command[9]));
                    cmd_index = 0;
                }
                else {
                    cmd_index++;
                }
            }
            else {
                cmd_index++;
            }
            
        }
        else {
            if ((circBuffer[ridx] == '\r') || (circBuffer[ridx] == '\n')) {
                
                if (cmd_index != 0)  {
                    command[cmd_index] = '\0';
                    parse_at_cmd(command);
                    cmd_index = 0;
                }
            }
            else if (cmd_index >= (CMD_SIZE - 1)) {
                cmd_index = 0;
                com_error(AT_TEST_PARAM_OVERFLOW);
            }
            else {
                command[cmd_index++] = circBuffer[ridx];
            }
            ridx = (ridx + 1) % CIRC_BUFF_SIZE;
        }
    }
   
    

}




void CMD_Process(void)
{
    CMD_AT_Process();
    
#if 0    
  if (circBuffOverflow == 1)
  {
    com_error(AT_TEST_PARAM_OVERFLOW);
    /*Full flush in case of overflow */
    UTILS_ENTER_CRITICAL_SECTION();
    ridx = widx;
    charCount = 0;
    circBuffOverflow = 0;
    UTILS_EXIT_CRITICAL_SECTION();
    i = 0;
  }

  while (charCount != 0)
  {
#if 0 /* echo On    */
    AT_PPRINTF("%c", circBuffer[ridx]);
#endif /* 0 */

    if (circBuffer[ridx] == AT_ERROR_RX_CHAR)
    {
      ridx++;
      if (ridx == CIRC_BUFF_SIZE)
      {
        ridx = 0;
      }
      UTILS_ENTER_CRITICAL_SECTION();
      charCount--;
      UTILS_EXIT_CRITICAL_SECTION();
      com_error(AT_RX_ERROR);
      i = 0;
    }
    else if ((circBuffer[ridx] == '\r') || (circBuffer[ridx] == '\n'))
    {
      ridx++;
      if (ridx == CIRC_BUFF_SIZE)
      {
        ridx = 0;
      }
      UTILS_ENTER_CRITICAL_SECTION();
      charCount--;
      UTILS_EXIT_CRITICAL_SECTION();

      if (i != 0)
      {
        command[i] = '\0';
        UTILS_ENTER_CRITICAL_SECTION();
        CMD_ProcessBackSpace(command);
        UTILS_EXIT_CRITICAL_SECTION();
        parse_cmd(command);
        i = 0;
      }
    }
    else if (i == (CMD_SIZE - 1))
    {
      i = 0;
      com_error(AT_TEST_PARAM_OVERFLOW);
    }
    else
    {
      command[i++] = circBuffer[ridx++];
      if (ridx == CIRC_BUFF_SIZE)
      {
        ridx = 0;
      }
      UTILS_ENTER_CRITICAL_SECTION();
      charCount--;
      UTILS_EXIT_CRITICAL_SECTION();
    }
  }

#endif

}




static void CMD_GetChar(uint8_t *rxChar, uint16_t size, uint8_t error)
{
    uint32_t next_widx = (widx + 1) % CIRC_BUFF_SIZE;
    
    if(next_widx != ridx) {
        circBuffer[widx] = *rxChar;
        widx = next_widx;
    }

    if (NotifyCb != NULL) {
        NotifyCb();
    }
}





static void parse_at_cmd(const char *cmd)
{
    int32_t     i;
    ATError_t  status = AT_OK;
    const struct ATCommand_s *Current_ATCommand;
    
    if ((cmd[0] != 'A') || (cmd[1] != 'T')) {
        status = AT_ERROR;
    }
    else if (cmd[2] == '\0') {
        
    }
    else if (cmd[2] == '?')
    {

    }
    else {

        status = AT_ERROR;
        cmd += 2;
    
        for (i = 0; i < (sizeof(ATCommand) / sizeof(struct ATCommand_s)); i++) {

            if (strncmp(cmd, ATCommand[i].string, ATCommand[i].size_string) == 0) {

                Current_ATCommand = &(ATCommand[i]);
                cmd += Current_ATCommand->size_string;

                /* parse after the command */
                switch (cmd[0]) {
                    case '\0':    /* nothing after the command */
                        status = Current_ATCommand->run(cmd);
                        break;

                    case '=':
                        if ((cmd[1] == '?') && (cmd[2] == '\0')) {
                            status = Current_ATCommand->get(cmd + 1);
                        }
                        else {
                            status = Current_ATCommand->set(cmd + 1);
                        }
                        break;
                        
                    case '?':
                        status = AT_OK;
                        break;

                    default:
                        break;
                }

                break;
            }
        }
    }

    com_error(status);

  
}

static void com_error(ATError_t error_type)
{
  /* USER CODE BEGIN com_error_1 */

  /* USER CODE END com_error_1 */
  if (error_type > AT_MAX)
  {
    error_type = AT_MAX;
  }
  AT_PRINTF(ATError_description[error_type]);
  /* USER CODE BEGIN com_error_2 */

  /* USER CODE END com_error_2 */
}

/* USER CODE BEGIN PrFD */

/* USER CODE END PrFD */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
