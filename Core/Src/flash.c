/*===========================================================================

 	 	 	 	 	 	 INCLUDE FILES FOR MODULE

 ===========================================================================*/
#include "main.h"
#include "flash.h"
#include "LoRaMacTypes.h"
#include "sys_app.h"
#include "se-identity.h"
#include <string.h>
#include "radio_driver.h"

/*===========================================================================

 DEFINITIONS AND DECLARATIONS FOR MODULE

 This section contains definitions for constants, macros, types, variables
 and other items needed by this module.

 ===========================================================================*/
#define REAL_EFS_SIZE						1024
#define MAX_EFS_MEMORY_SIZE					((REAL_EFS_SIZE/8)*8)

/*===========================================================================

                        LOCAL DEFINITIONS

===========================================================================*/
static uint8_t		            EFS_LOAD_BUFFER[MAX_EFS_MEMORY_SIZE+8];

/*===========================================================================

                        FUNCTION PROTOTYPES

===========================================================================*/
sEfsItemType *GetFLASH(void)
{
	sEfsItemType *pRet = (sEfsItemType*)EFS_LOAD_BUFFER;
	return pRet;
}

/*===========================================================================

 FUNCTION FLASH_EFS_FactoryReset

 DESCRIPTION
 This is a factory reset.
 PARAMETERS
 None.

 RETURN VALUE
 None.

 DEPENDENCIES
 None.

 SIDE EFFECTS
 None.
 ===========================================================================*/
void FLASH_EFS_FactoryReset()
{
    sEfsItemType *pEfsData;
    
    pEfsData = GetFLASH();
    memset(pEfsData, 0, sizeof(sEfsItemType));

    pEfsData->dwEfsInit = EFS_INIT_DONE;
    pEfsData->dwEfsSize = sizeof(sEfsItemType);

    pEfsData->uTrimValA = 11;
    pEfsData->uTrimValB = 13;
    
    pEfsData->u8NodeId = 1;
    pEfsData->u8Rateset = 1;


}


/*===========================================================================

 FUNCTION MX_FLASH_Init

 DESCRIPTION
   Initialize internal flash memory.

 PARAMETERS
 None.

 RETURN VALUE
 None.

 DEPENDENCIES
 None.

 SIDE EFFECTS
 None.
 ===========================================================================*/
void MX_FLASH_Init(void)
{
	sEfsItemType *pEfsData;

	FLASH_EFS_Read();

    pEfsData = GetFLASH();

    if(pEfsData->dwEfsInit != EFS_INIT_DONE || pEfsData->dwEfsSize != sizeof(sEfsItemType))
    {
        FLASH_EFS_FactoryReset();
        FLASH_EFS_Write();
    }

    if(pEfsData->dwResetReson != 0)
	{
		pEfsData->dwResetReson = 0;
		FLASH_EFS_Write();
	}

}

/*===========================================================================

 FUNCTION FLASH_EFS_Erase

 DESCRIPTION
   Internal flash memory erase.

 PARAMETERS
 None.

 RETURN VALUE
 None.

 DEPENDENCIES
 None.

 SIDE EFFECTS
 None.
 ===========================================================================*/
uint32_t FLASH_EFS_Erase(void)
{
	uint32_t dwPageCount;
	FLASH_EraseInitTypeDef desc;
	uint32_t result = FLASHIF_OK;

	/* Unlock the FLASH control register access */
	HAL_FLASH_Unlock();

	dwPageCount = FLASH_SIZE / FLASH_PAGE_SIZE;
	desc.Page = dwPageCount-1;
	desc.NbPages = 1;

	desc.TypeErase = FLASH_TYPEERASE_PAGES;

	/* Clear the FLASH's pending flags. */
	__HAL_FLASH_CLEAR_FLAG(FLASH_FLAG_OPTVERR | FLASH_FLAG_CFGBSY);

	/* Perform a mass erase or erase the specified FLASH memory pages */
	if (HAL_FLASHEx_Erase(&desc, &result) != HAL_OK)
	{
		result = FLASHIF_ERASEKO;
	}
	else
	{
		result = FLASHIF_OK;
	}

	 /* Lock the FLASH control register access */
	HAL_FLASH_Lock();

	return result;
}

/*===========================================================================

 FUNCTION FLASH_EFS_Write

 DESCRIPTION
   Internal flash memory write.
   
 PARAMETERS
 None.

 RETURN VALUE
 None.

 DEPENDENCIES
 None.

 SIDE EFFECTS
 None.
 ===========================================================================*/
uint32_t FLASH_EFS_Write(void)
{
	uint32_t dwPageCount;
	uint32_t destination;
	HAL_StatusTypeDef status;

    uint32_t result = FLASHIF_OK;
    uint64_t *p_actual = (uint64_t*)EFS_LOAD_BUFFER;
    uint32_t size = MAX_EFS_MEMORY_SIZE / 8;

    result = FLASH_EFS_Erase();
    if(result != FLASHIF_OK) {
    	return result;
    }

    HAL_FLASH_Unlock();

    dwPageCount = (FLASH_SIZE / FLASH_PAGE_SIZE);
    destination = (dwPageCount-1) * FLASH_PAGE_SIZE;

    for(int i=0; i<size; i++) {

    	/* Write the buffer to the memory */
    	__HAL_FLASH_CLEAR_FLAG(FLASH_FLAG_OPTVERR | FLASH_FLAG_CFGBSY | FLASH_SR_PROGERR | FLASH_SR_FASTERR);

    	status = HAL_FLASH_Program(FLASH_TYPEPROGRAM_DOUBLEWORD, destination, (uint64_t)*p_actual);

        if (status == HAL_OK) {
            /* Increment the memory pointers */
            destination += (8);
            p_actual += 1;
        }
        else {
        	result = FLASHIF_WRITING_ERROR;
        }

        if ( status != HAL_OK ) {
            break;
        }
    }

    HAL_FLASH_Lock();
    HAL_Delay(50);
    
    return result;
}


uint32_t FLASH_EFS_Read(void)
{
	uint32_t dwPageCount;
	uint32_t destination;

    uint32_t result = FLASHIF_OK;
    uint64_t *p_actual = (uint64_t*)EFS_LOAD_BUFFER;
    uint32_t size = MAX_EFS_MEMORY_SIZE / 8;

    HAL_FLASH_Unlock();

    dwPageCount = (FLASH_SIZE / FLASH_PAGE_SIZE);
    destination = (dwPageCount-1) * FLASH_PAGE_SIZE;

    for(int i=0; i<size; i++) {

    	*p_actual = (*(__IO uint64_t*)destination);

		/* Increment the memory pointers */
		destination += (8);
		p_actual += 1;

    }

    HAL_FLASH_Lock();


    return result;
}
