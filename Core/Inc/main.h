/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under Ultimate Liberty license
  * SLA0044, the "License"; You may not use this file except in compliance with
  * the License. You may obtain a copy of the License at:
  *                             www.st.com/SLA0044
  *
  ******************************************************************************
  */
#ifdef __cplusplus
extern "C" {
#endif

/*===========================================================================

                       INCLUDE FILES FOR MODULE

===========================================================================*/

#include "stm32wlxx_hal.h"
#include "string.h"

/*===========================================================================

                        DATA DECLARATIONS

===========================================================================*/
/* Type directive for data belonging in the "Notice" segment
*/
#define NOTICE_TYPE       const char

/* Define 'SHOW_STAT' in order to view static's as globals
** (e.g. cl /DSHOW_STAT foo.c) If 'SHOW_STAT' is not defined,
** it gets defined as 'static'
*/
#ifdef LOCAL
#undef LOCAL
#endif

#ifdef SHOW_STAT
  #define LOCAL
#else
  #define LOCAL static
#endif
typedef uint32_t   dword;
typedef uint16_t   word;
typedef uint8_t    byte;
typedef  unsigned char      boolean;     /* Boolean value type. */

/*===========================================================================

                DEFINITIONS AND DECLARATIONS FOR MODULE

This section contains definitions for constants, macros, types, variables
and other items needed by this module.

===========================================================================*/

/*===========================================================================

                         DEFINE DEFINITIONS

===========================================================================*/
#if 0
#define RTC_N_PREDIV_S 					10
#define RTC_PREDIV_S 					((1<<RTC_N_PREDIV_S)-1)
#define RTC_PREDIV_A 					((1<<(15-RTC_N_PREDIV_S))-1)
#define LPUART_BAUDRATE 				9600
#define SPI1_SCK_Pin 					GPIO_PIN_3
#define SPI1_SCK_GPIO_Port 				GPIOB
#define SPI1_MISO_Pin 					GPIO_PIN_4
#define SPI1_MISO_GPIO_Port 			GPIOB
#define SPI1_MOSI_Pin 					GPIO_PIN_5
#define SPI1_MOSI_GPIO_Port 			GPIOB
#define I2C1_SCL_Pin 					GPIO_PIN_6
#define I2C1_SCL_GPIO_Port 				GPIOB
#define I2C1_SDA_Pin 					GPIO_PIN_7
#define I2C1_SDA_GPIO_Port 				GPIOB
#define WAKEUP1_IN_Pin 					GPIO_PIN_0
#define WAKEUP1_IN_GPIO_Port 			GPIOA
#define GPIO_PA1_IN_Pin 				GPIO_PIN_1
#define GPIO_PA1_IN_GPIO_Port 			GPIOA
#define LPUART1_TX_Pin 					GPIO_PIN_2
#define LPUART1_TX_GPIO_Port 			GPIOA
#define LPUART1_RX_Pin 					GPIO_PIN_3
#define LPUART1_RX_GPIO_Port 			GPIOA
#define GPIO_PA4_IN_Pin 				GPIO_PIN_4
#define GPIO_PA4_IN_GPIO_Port 			GPIOA
#define GPIO_PA5_IN_Pin 				GPIO_PIN_5
#define GPIO_PA5_IN_GPIO_Port 			GPIOA
#define GPIO_PA6_IN_Pin 				GPIO_PIN_6
#define GPIO_PA6_IN_GPIO_Port 			GPIOA
#define GPIO_PA7_IN_Pin 				GPIO_PIN_7
#define GPIO_PA7_IN_GPIO_Port 			GPIOA
#define GPIO_PA8_IN_Pin 				GPIO_PIN_8
#define GPIO_PA8_IN_GPIO_Port 			GPIOA
#define USART1_TX_Pin 					GPIO_PIN_9
#define USART1_TX_GPIO_Port 			GPIOA
#define SPI1_NSS_Pin 					GPIO_PIN_2
#define SPI1_NSS_GPIO_Port 				GPIOB
#define GPIO_PB12_IN_Pin 				GPIO_PIN_12
#define GPIO_PB12_IN_GPIO_Port 			GPIOB
#define USART_RX_Pin 					GPIO_PIN_10
#define USART_RX_GPIO_Port 				GPIOA
#define RF_SW_VDD_OUT_Pin 				GPIO_PIN_11
#define RF_SW_VDD_OUT_GPIO_Port 		GPIOA
#define ADC_IN8_IN_Pin 					GPIO_PIN_12
#define ADC_IN8_IN_GPIO_Port 			GPIOA
#define DEBUG_SWDIO_Pin 				GPIO_PIN_13
#define DEBUG_SWDIO_GPIO_Port 			GPIOA
#define RF_SW_CTL_OUT_Pin 				GPIO_PIN_13
#define RF_SW_CTL_OUT_GPIO_Port 		GPIOC
#define OSC_32K_IN_Pin 					GPIO_PIN_14
#define OSC_32K_IN_GPIO_Port 			GPIOC
#define OSC_32K_OUT_Pin 				GPIO_PIN_15
#define OSC_32K_OUT_GPIO_Port 			GPIOC
#define DEBUG_SWCLK_Pin 				GPIO_PIN_14
#define DEBUG_SWCLK_GPIO_Port 			GPIOA
#define ADC_IN11_IN_Pin 				GPIO_PIN_15
#define ADC_IN11_IN_GPIO_Port 			GPIOA
#else
#define RTC_N_PREDIV_S 					10
#define RTC_PREDIV_S 					((1<<RTC_N_PREDIV_S)-1)
#define RTC_PREDIV_A 					((1<<(15-RTC_N_PREDIV_S))-1)
#define LPUART_BAUDRATE 				9600

#define ADC_TEMP1_IN_Pin 				GPIO_PIN_15
#define ADC_TEMP1_IN_GPIO_Port 			GPIOA

#define ADC_TEMP2_IN_Pin 				GPIO_PIN_3
#define ADC_TEMP2_IN_GPIO_Port 			GPIOB

#define GPIO_PIN_4_Pin 					GPIO_PIN_4
#define GPIO_PIN_4_GPIO_Port 			GPIOB
#define GPIO_PIN_5_Pin 					GPIO_PIN_5
#define GPIO_PIN_5_GPIO_Port 			GPIOB
#define UART1_TX_DBG_OUT_Pin			GPIO_PIN_6
#define UART1_TX_DBG_OUT_GPIO_Port 		GPIOB
#define UART1_RX_DBG_IN_Pin 			GPIO_PIN_7
#define UART1_RX_DBG_IN_GPIO_Port 		GPIOB
#define GPIO_PIN_8_Pin 				    GPIO_PIN_8
#define GPIO_PIN_8_Port 				GPIOB
#define RF_SW_CTL_OUT_Pin 				GPIO_PIN_0
#define RF_SW_CTL_OUT_GPIO_Port 		GPIOA
#define GPIO_PA1_IN_Pin 				GPIO_PIN_1
#define GPIO_PA1_IN_GPIO_Port 			GPIOA
#define LPUART1_TX_Pin 					GPIO_PIN_2
#define LPUART1_TX_GPIO_Port 			GPIOA
#define LPUART1_RX_Pin 					GPIO_PIN_3
#define LPUART1_RX_GPIO_Port 			GPIOA
#define SPI1_NSS_IN_Pin 				GPIO_PIN_4
#define SPI1_NSS_IN_GPIO_Port 			GPIOA
#define SPI1_SCK_IN_Pin 				GPIO_PIN_5
#define SPI1_SCK_IN_GPIO_Port 			GPIOA
#define SPI1_MISO_IN_Pin 				GPIO_PIN_6
#define SPI1_MISO_IN_GPIO_Port 			GPIOA
#define SPI1_MOSI_IN_Pin 				GPIO_PIN_7
#define SPI1_MOSI_IN_GPIO_Port 			GPIOA

#define PWM_OUT_LED_Pin 				GPIO_PIN_8
#define PWM_OUT_LED_GPIO_Port 			GPIOA

#define GPIO_PIN_9_Pin 					GPIO_PIN_9
#define GPIO_PIN_9_GPIO_Port 			GPIOA

#define WAKE_UP2_IN_Pin 				GPIO_PIN_2
#define WAKE_UP2_IN_GPIO_Port 			GPIOB
#define WAKEUP1_IN_Pin 				    GPIO_PIN_12
#define WAKEUP1_IN_GPIO_Port 			GPIOB

#define RF_SW_VDD_OUT_Pin 				GPIO_PIN_10
#define RF_SW_VDD_OUT_GPIO_Port 		GPIOA

#define I2C2_SDA_OUT_Pin			    GPIO_PIN_11
#define I2C2_SDA_OUT_Port 		        GPIOA

#define I2C2_SCL_OUT_Pin 				GPIO_PIN_12
#define I2C2_SCL_GPIO_Port 			    GPIOA

#define DEBUG_SWDIO_Pin 				GPIO_PIN_13
#define DEBUG_SWDIO_GPIO_Port 			GPIOA
#define GPIO_PIN_13_Pin 				GPIO_PIN_13
#define GPIO_PIN_13_Port 		        GPIOC
#define OSC_32K_IN_Pin 					GPIO_PIN_14
#define OSC_32K_IN_GPIO_Port 			GPIOC
#define OSC_32K_OUT_Pin 				GPIO_PIN_15
#define OSC_32K_OUT_GPIO_Port 			GPIOC
#define DEBUG_SWCLK_Pin 				GPIO_PIN_14
#define DEBUG_SWCLK_GPIO_Port 			GPIOA



#endif

/*===========================================================================

                        TYPE DEFINITIONS

===========================================================================*/


/*===========================================================================

                        LOCAL DEFINITIONS

===========================================================================*/


/*===========================================================================

                        GLOBAL VARIABLE

===========================================================================*/
/* Compile and Release Date/Time
*/
extern NOTICE_TYPE ver_date[];       /* Compile date string        */
extern NOTICE_TYPE ver_time[];       /* Compile time string        */
extern NOTICE_TYPE ver_dir[];        /* Compile directory string   */
extern NOTICE_TYPE ver_sw[];         /* Release S/W versin string  */
extern NOTICE_TYPE ver_hw[];         /* Release H/W versin string  */
extern const int   ver_ndev;

extern NOTICE_TYPE rel_date[];       /* Release date string        */
extern NOTICE_TYPE rel_time[];       /* Release time string        */

/*===========================================================================

                        FUNCTION PROTOTYPES

===========================================================================*/
void Error_Handler(void);




void SystemClock_Config(void);

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
